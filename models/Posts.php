<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "posts".
 *
 * @property int $id
 * @property int $c_user_id
 * @property string $post_desc
 * @property string $created_at
 */
class Posts extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'posts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['c_user_id'], 'integer'],
            [['post_desc'], 'string'],
            [['post_desc'], 'required', 'message'=> ''],
            [['created_at'], 'safe'],
        ];
    }



    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'c_user_id' => 'C User ID',
            'post_desc' => 'Post Desc',
            'created_at' => 'Created At',
        ];
    }

    public function cre(){
            $p = new Posts();
            if($this->post_desc){
                $user_id = Yii::$app->user->id;
                $p->c_user_id = $user_id;
                $p->post_desc = $this->post_desc;
                $p->save();
            }

    }
}
