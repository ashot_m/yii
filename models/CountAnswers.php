<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "count_answers".
 *
 * @property int $id
 * @property int $teacher_id
 * @property int $count_right_ansers
 */
class CountAnswers extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'count_answers';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['teacher_id', 'count_right_ansers'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'teacher_id' => 'Teacher ID',
            'count_right_ansers' => 'Count Right Ansers',
        ];
    }
}
