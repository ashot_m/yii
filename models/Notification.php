<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "notification".
 *
 * @property int $id
 * @property int $from
 * @property int $to
 * @property int $course_id
 * @property int $seen_not_seen
 */
class Notification extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'notification';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['from', 'to', 'course_id', 'seen_not_seen'], 'integer'],
            [['notifi'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'from' => 'From',
            'to' => 'To',
            'course_id' => 'Course ID',
            'notifi' => 'Notifi',
            'seen_not_seen' => 'Seen Not Seen',
        ];
    }

    public function addNoty($arg, $arg1){
        $user_id = Yii::$app->user->id;
        $query1 = new Notification();
        $query1->to = $arg;
        $query1->from = $user_id;
        $query1->course_id = $arg1;
        $query1->save();
    }

    public function sayYas($arg,$arg1,$arg2,$arg3){
        $model = Notification::findOne($arg);
        $model->seen_not_seen = 1;
        $model->update();
        $user_id = Yii::$app->user->id;
        $query1 = new Notification();
        $query1->to = $arg2;
        $query1->from = $user_id;
        $query1->course_id = $arg3;
        $query1->notifi = $arg1;
        $query1->save();
    }

    public function sayNo($arg,$arg1,$arg2,$arg3){
        $user_id = Yii::$app->user->id;
        $query1 = new Notification();
        $query1->notifi = $arg1;
        $query1->to = $arg2;
        $query1->from = $user_id;
        $query1->course_id = $arg3;
        $query1->save();
        $model = Notification::findOne($arg);
        $model->seen_not_seen = 1;
        $model->update();
        $course_student = CourseStudent::findOne(['course_id' => $arg3, 'student_id'=>$arg2]);
        $course_student->delete();
        $teacher_student = TeacherStudent::findOne(['teacher_id' => $user_id, 'student_id'=>$arg2]);
        $teacher_student->delete();

    }
}
