<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "reviews".
 *
 * @property int $id
 * @property int $from
 * @property int $to
 * @property string $text
 * @property string $created_at
 */
class Reviews extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'reviews';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['from', 'to'], 'integer'],
            [['text'], 'string'],
            [['created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'from' => 'From',
            'to' => 'To',
            'text' => 'Text',
            'created_at' => 'Created At',
        ];
    }

    public function add($arg1, $arg2, $arg3){
        $model = new Reviews();
        $model->from = $arg1;
        $model->to = $arg2;
        $model->text = $arg3;
        $model->save();

    }
}
