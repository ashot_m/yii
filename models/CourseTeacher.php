<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "course_teacher".
 *
 * @property int $id
 * @property int $course_id
 * @property int $teacher_id
 */
class CourseTeacher extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'course_teacher';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['course_id', 'teacher_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'course_id' => 'Course ID',
            'teacher_id' => 'Teacher ID',
        ];
    }

    public function teacher($arg){
        $query1 = CourseTeacher::find();
        $query1->select('teacher_id')->where(['course_id' => $arg]);
        $teachers = $query1->all();
        return $teachers;
    }
}
