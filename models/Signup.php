<?php

namespace app\models;

use yii\base\Model;


class Signup extends Model{
    public $first_name;
    public $last_name;
    public $email;
    public $phone;
    public $password;
    public $role;

    public function rules()
    {

        return [
            [['first_name', 'last_name', 'email', 'password', 'phone', 'role'], 'required', 'message'=>'Լրացրե՛ք դաշտը'],
            [['role'], 'integer'],
            ['email', 'unique', 'targetClass'=> 'app\models\User', 'message'=>'Այս Էլ-հասցեն արդեն գրանցված է'],
            [['first_name', 'last_name', 'email', 'password', 'phone'], 'string', 'max' => 255],
        ];
    }


    public function signup(){
        $user = new User();
        $user->first_name = $this->first_name;
        $user->last_name = $this->last_name;
        $user->email = $this->email;
        $user->phone = $this->phone;
        $user->password = $this->password;
        $user->role = $this->role;
        $user->Save();
    }
}
