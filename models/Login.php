<?php

namespace app\models;

use yii\base\Model;
use app\models\User;
class Login extends Model{

    public $email;
    public $password;

    public function rules()
    {
        return [
            [['email','password'], 'required'],
            ['email','email'],
            ['password', 'validatePassword']
        ];
    }

    public function validatePassword($attribute, $params){
        if(!$this->hasErrors()){
        $user = $this->getUser();
        if(!$user || ($user->password != $this->password)){
            $this->addError($attribute, 'Գաղտնաբառը կամ Էլ-հասցեն սխալ է մուտքագրած');
        }
        }
    }

    public function getUser(){
        return User::findOne(['email'=>$this->email]);
    }

}
