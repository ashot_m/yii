<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "teacher_student".
 *
 * @property int $id
 * @property int $teacher_id
 * @property int $student_id
 */
class TeacherStudent extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'teacher_student';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['teacher_id', 'student_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'teacher_id' => 'Teacher ID',
            'student_id' => 'Student ID',
        ];
    }

    public function TeacherStudent($arg){
        $user_id = Yii::$app->user->id;
        $query = new TeacherStudent();
        $query->teacher_id = $arg;
        $query->student_id = $user_id;
        $query->save();
    }
}
