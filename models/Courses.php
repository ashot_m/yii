<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "courses".
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $duration
 * @property string $icon
 */
class Courses extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'courses';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['duration'], 'integer'],
            [['name', 'icon'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 500],
            [['description_long'], 'string', 'max' => 1000],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'description_long' => 'Description Long',
            'duration' => 'Duration',
            'icon' => 'Icon',
        ];
    }

    public function check($arg){
        $query = Courses::find();
        $query->select('*')->where(['id' => $arg]);
        $cours = $query->one();
        return $cours;
    }
}
