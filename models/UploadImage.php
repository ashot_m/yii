<?php
namespace app\models;

use Yii;
use yii\base\Controller;
use yii\base\Model;
use yii\web\UploadedFile;
use yii\db\ActiveRecord;
use yii\base\Configurable;
use yii\web\IdentityInterface;
use app\models\User;




class UploadImage extends Model{

    public $prof_img;
    public function rules(){
        return[
            [['prof_img'], 'file', 'extensions' => 'png, jpg, jpeg'],
        ];
    }

    public function upload(){
        if($this->validate()){
            $img = $this->prof_img->saveAs("uploads/{$this->prof_img->baseName}.{$this->prof_img->extension}");
            $user_id = Yii::$app->user->id;
            $user = User::find()->where(['id' => $user_id])->one();
            $user->prof_img = $this->prof_img->name;
            $user->save();
        }else{
            return false;
        }
    }

}