<?php

namespace app\models;

use Yii;
use yii\web\IdentityInterface;
use yii\db\ActiveRecord;
use app\models\Login;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $password
 * @property string $phone
 * @property int $role
 */
class User extends ActiveRecord implements IdentityInterface
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'email', 'password', 'phone', 'role'], 'required'],
            [['role'], 'integer'],
            [['first_name', 'last_name', 'email', 'password', 'phone', 'prof_img', 'about_me'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'password' => 'Password',
            'phone' => 'Phone',
            'prof_img'=>'Prof_img',
            'about_me'=> 'about_me',
            'role' => 'Role',
        ];
    }

    public function edit(){
        $user_id = Yii::$app->user->id;
        $user = User::find()->where(['id' => $user_id])->one();
        $user->first_name = $this->first_name;
        $user->last_name = $this->last_name;
        $user->email = $this->email;
        $user->phone = $this->phone;
        $user->about_me = $this->about_me;
        $user->save();
    }

    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    public function getId()
    {
        return $this->id;
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {

    }


    public function getAuthKey()
    {

    }

    public function validateAuthKey($authKey)
    {

    }

    public function checkTeacher($ar1){
        $teacher = new CourseTeacher();
        $te_ids = $teacher->teacher($ar1);
        $query2 = User::find();
        $arr = [];
        for($i = 0; $i < count($te_ids); $i++){
            $teach = $query2->select('*')->where(['id' => $te_ids[$i]])->all();
            $arr['ids'][$i] = $teach[0]->id;
            $arr['names'][$i] = $teach[0]->first_name. ' ' .$teach[0]->last_name;
        }

            return $arr;



}
}
