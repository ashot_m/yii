<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "course_student".
 *
 * @property int $id
 * @property int $course_id
 * @property int $student_id
 */
class CourseStudent extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'course_student';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['course_id', 'student_id'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'course_id' => 'Course ID',
            'student_id' => 'Student ID',
        ];
    }

    public function CourseStudent($arg){
        $student_id = Yii::$app->user->id;
        $query = new CourseStudent();
        $query->course_id = $arg;
        $query->student_id = $student_id;
        $query->save();
    }
}
