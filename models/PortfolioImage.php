<?php
namespace app\models;

use Yii;
use yii\base\Controller;
use yii\base\Model;
use yii\web\UploadedFile;
use yii\db\ActiveRecord;
use yii\base\Configurable;
use yii\web\IdentityInterface;
use app\models\User;
use app\models\UploadImage;




class PortfolioImage extends Model{

    public $portfolio_img;
    public function rules(){
        return[
            [['portfolio_img'], 'file', 'extensions' => 'png, jpg, jpeg'],
        ];
    }

    public function upload(){
        if($this->validate()){
            $query = new Portfolio();
            $this->portfolio_img->saveAs("uploads/portfolio/{$this->portfolio_img->baseName}.{$this->portfolio_img->extension}");
            $name = $this->portfolio_img->name;
            return $name;
        }else{
            return false;
        }
    }

}