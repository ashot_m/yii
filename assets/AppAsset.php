<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/bootstrap.min.css',
        'css/font-awesome.min.css',
        'css/font-awesome.css',
        'css/animate.min.css',
        'css/owl.carousel.css',
        'css/owl.transitions.css',
        'css/prettyPhoto.css',
        'css/jquery.formstyler.theme.css',
        'css/jquery.formstyler.css',
        'css/main.css',
        'css/style.css'
    ];
    public $js = [
        'js/jquery.js',
        'js/bootstrap.min.js',
        'js/owl.carousel.min.js',
        'js/mousescroll.js',
        'js/smoothscroll.js',
        'js/jquery.prettyPhoto.js',
        'js/jquery.isotope.min.js',
        'js/jquery.inview.min.js',
        'js/wow.min.js',
        'js/ajax-modal-popup.js',
        'js/jquery.formstyler.min.js',
        'js/main.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
