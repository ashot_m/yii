<?php

use app\models\UploadImage;
use yii\helpers\Html;
use app\assets\AppAsset;
use yii\bootstrap\Modal;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;
use app\models\Signup;
use app\models\User;
use app\controllers\SiteController;
use yii\db\ActiveRecord;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\filters\AjaxFilter;
use app\models\Courses;
use yii\web\JsExpression;
use app\models\Posts;
use app\models\CourseStudent;
use \app\models\CourseTeacher;
use app\models\Portfolio;
use app\models\Notification;
use app\models\Chat;

$user_id = Yii::$app->request->get('id');
$lodined_id = Yii::$app->user->id;



$CourseStudent = new CourseStudent();
$query3 = CourseStudent::find()->where(['student_id'=> $lodined_id]);
$res = $query3->all();
if(!$res){
    $flag = 0;
}else{
    $flag = 1;
}



$model = new UploadImage();
if(Yii::$app->request->isPost && Yii::$app->request->post('UploadImage')){
    $model->prof_img = UploadedFile::getInstance($model, 'prof_img');
    $model->upload();
}



$model1 = new Courses();
$query1 = Courses::find();
$courses = $query1->select('*')->all();
$courses_lenght = count($courses);

$user = new User();
$query = User::find();
$query->where(['id' => $user_id]);
$user_1 = $query->one();

$query_new = User::find()->where(['id' => $lodined_id]);;
$user_logined = $query_new->one();




$teachers = $query->where(['role' => '1'])->all();

AppAsset::register($this);
$post_model = new Posts();
if(Yii::$app->request->isPost){
    $post_model->cre();
}

$post_model1 = Posts::find();
$query2 = $post_model1->select('*');
$post = $query2->where(['c_user_id' => $user_id])->orderBy([ 'id' => SORT_DESC])->all();


$checked_courses = [];
for($q = 0; $q < count($res); $q++){
    $query5 = Courses::find()->select('*')->where(['id' => $res[$q]['course_id']]);
    $c = $query5->one();
    $checked_courses[] = $c;
}

$portfolio_model = new Portfolio();

$query9 = Portfolio::find()->select('*')->where(['teacher_id'=> $user_id]);
$portfolio_work = $query9->all();

$notifi_query = Notification::find()->select('*')->where(['to' => $lodined_id, 'seen_not_seen' => '0']);
$notification = $notifi_query->all();
$count_noti = count($notification);

$notifi_model = new Notification();


AppAsset::register($this);
?>

<?php $this->beginPage();?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <title><?= $user_1['first_name'] .' '. $user_1['last_name'] ?></title>
    <?php $this->head() ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?= Html::csrfMetaTags(); ?>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <!-- CSS -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="web/images/ico/logo.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body id="home" class="homepage prof_home_page">
<?php $this->beginBody() ?>

<header id="header" class="prof_header">
    <nav id="main-menu" class="navbar navbar-default navbar-fixed-top" role="banner">
        <div class="container prof_cont">
            <div class="navbar-header prof-navbar-header">
                <a class="navbar-brand" href="<?php echo Yii::$app->request->baseUrl . '/site/profile?id='. $lodined_id ?>"><img src="<?php echo Yii::$app->request->baseUrl.'/images/' . 'logo.png'?>" alt="logo"></a>
            </div>
            <div class="header_right">
                <ul class="messige_ul">
                    <li>
                        <a href="" class="messige_link">
                            <i class="fa fa-envelope"></i>
                        </a>
                    </li>
                    <li class="str_li">
                        <div class="notify" data-userid="<?php if($notification) echo $lodined_id; ?>">
                            <i class="fa fa-bell"></i>
                            <?php if($notification) : ?>
                                <span class="notifi_count"><?= $count_noti ?></span>
                            <?php endif; ?>
                        </div>
                        <ul class="notification_list">
                            <?php if($notification) : ?>
                                <?php for($l = 0; $l < $count_noti; $l++) : ?>
                                    <?php if($notification[$l]['seen_not_seen'] == '0') : ?>
                                        <li data-notid="<?= $notification[$l]['id'] ?>" class="noti_list">
                                            <?php
                                            $query10 = User::find()->select('first_name')->where(['id' => $notification[$l]['from']]);
                                            $user_name = $query10->one();
                                            $query11 = Courses::find()->select('name')->where(['id' => $notification[$l]['course_id']]);
                                            $course_name = $query11->one();
                                            ?>
                                            <?php if($user_logined['role'] == '1') : ?>
                                                <p class="notifi_text"><a href="<?php echo Yii::$app->request->baseUrl . '/site/profile?id='. $notification[$l]['from'] ?>"><?= $user_name->first_name ?></a> անունով ուսանողը ընտրել է ձեզ <strong><?= $course_name->name; ?></strong> դասընթացը դասավանդելու համար:</p>
                                                <?php $form3 = ActiveForm::begin(['id' => 'notifi_form']) ?>
                                                <?= $form3->field($notifi_model, 'notifi')->textarea(['placeholder'=>'Մերժելու դեպքում գրե՛ք պատճառը'])->label(''); ?>
                                                <div class="notifi_buttons">
                                                    <button data-what="1" data-notifi="<?= $notification[$l]['id'] ?>" data-to1="<?= $notification[$l]['from'] ?>" data-cid1="<?=$notification[$l]['course_id']?>" class="notifi_button">Ընդունել</button>
                                                    <button data-what="0" data-notifi="<?= $notification[$l]['id'] ?>" data-to="<?= $notification[$l]['from'] ?>" data-cid="<?=$notification[$l]['course_id']?>" class="notifi_button">Չեղարկել</button>
                                                </div>
                                                <?php $form3 = ActiveForm::end() ?>
                                            <?php else :?>
                                                <?php if($notification[$l]['notifi'] !== '1') : ?>
                                                    <p class="notifi_text"><a href="<?php echo Yii::$app->request->baseUrl . '/site/profile?id='. $notification[$l]['from'] ?>"><?= $user_name->first_name ?></a> անունով դասախոսը մերժել է ձեզ <strong><?= $course_name->name; ?></strong> դասընթացը ձեզ դասավանդելու համար:</p>
                                                    <p>Պատճառը՝ <?= $notification[$l]['notifi'] ?></p>
                                                <?php else :?>
                                                    <p class="notifi_text"><a href="<?php echo Yii::$app->request->baseUrl . '/site/profile?id='. $notification[$l]['from'] ?>"><?= $user_name->first_name ?></a> անունով դասախոսը ընդունել է ձեր հրավերը <strong><?= $course_name->name; ?></strong> դասընթացը ձեզ դասավանդելու համար:</p>
                                                <?php endif;?>
                                            <?php endif;?>
                                        </li>
                                    <?php endif ?>
                                <?php endfor; ?>

                        </ul>
                    </li>
                </ul>
                <ul class="prof_ul">
                    <li class="pos">
                        <div class="prof_set">
                            <div class="prof_set_img">
                                <?php if($user_logined['prof_img'] ): ?>
                                    <img src="/uploads/<?= $user_logined['prof_img'] ?>" alt="">
                                <?php else: ?>
                                    <img src="https://wowsciencecamp.org/wp-content/uploads/2018/07/dummy-user-img-1-400x400_x_acf_cropped.png" alt="">
                                <?php endif; ?>
                            </div>

                            <div class="prof_name">
                                <p class="icon_name"><?= $user_logined['first_name'] .' '. $user_logined['last_name']?></p><i class="fa fa-sort-down"></i>
                            </div>
                        </div>
                        <ul class="prof_ul_none">
                            <li>
                                <a href="#">Կարգավորումներ</a>
                            </li>
                            <li>
                                <a href="<?= Url::to(['site/logout'])?>"> Դուրս գալ</a>
                            </li>
                        </ul>
                    </li>
                </ul>
                <?php endif; ?>
            </div>
        </div><!--/.container-->
    </nav><!--/nav-->
</header><!--/header-->


<?php
    $chat_model = new Chat()
?>

<?php $form = ActiveForm::begin() ?>
<?= $form->field($chat_model, 'text')->textarea(); ?>
<?= $form->field($chat_model, 'file')->fileInput(); ?>
<?php $form = ActiveForm::end(); ?>

<div class="chat_block">
    <div class="chat_block_content">
        <div class="chat">

        </div>
    </div>
</div>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>