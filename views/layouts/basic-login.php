<?php

use app\models\Signup;
use yii\helpers\Html;
use app\assets\AppAsset;
use yii\widgets\ActiveForm;
use app\models\Login;
use app\models\User;
use app\controllers\SiteController;
use yii\db\ActiveRecord;


AppAsset::register($this);

$login_model = new Login();
if(!Yii::$app->user->isGuest){
    return $this->goHome();
}
if(Yii::$app->request->post('Login')){
    $login_model->attributes = Yii::$app->request->post('Login');

    if($login_model->validate()){
       Yii::$app->user->login($login_model->getUser());
       return $this->goHome();
    }
}

if(!Yii::$app->user->isGuest){
    Yii::$app->user->logout();
    return $this->redirect(['login']);
}

$model = new Signup();

if(isset($_POST['Signup'])){
    $model->attributes = Yii::$app->request->post('Signup');
}

if($model->validate() && $model -> signup()){
    return $this->goHome();
}

?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <?= Html::csrfMetaTags() ?>
    <title>Մուտք</title>
    <?php $this->head() ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <!-- CSS -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="web/images/ico/logo.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body id="home" class="homepage login_page" >
<?php $this->beginBody() ?>

<header id="header">
    <nav id="main-menu" class="navbar navbar-default navbar-fixed-top" role="banner">
        <div class="container" style="text-align: center">
            <div class="navbar-header" style="float: right; text-align: center">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html" ><img src="<?php echo Yii::$app->request->baseUrl.'/images/' . 'logo.png'?>" alt="logo"></a>
            </div>
        </div><!--/.container-->
    </nav><!--/nav-->
</header><!--/header-->
<div class="login_content">
    <h3 style="text-align: center; color: #ceaee8;">Մուտք գործել</h3>
    <?php $form = ActiveForm::begin()?>

    <?= $form->field($login_model, 'email')->textInput(['type'=> 'email'])->label('Էլ-հասցե')?>
    <?= $form->field($login_model, 'password')->textInput(['type'=>'password'])->label('Գաղտնաբառ')?>
    <div style="text-align: center">
        <button type="submit" class="btn btn-primary btn-lg">Մուտք</button>
    </div>
</div>


<?php $form = ActiveForm::end()?>

<div class="container">
</div>
<div class="overlay">
</div>
<div class="main-popup">
    <div class="popup-header">
        <div id="popup-close-button"><a href="#"></a></div>
        <ul>
            <li><a href="#" id="sign-in">Գրանցվել որպես ուսանող</a></li>
            <li><a href="#" id="register">Գրանցվել որպես Դասավանդող</a></li>
        </ul>
    </div><!--.popup-header-->
    <div class="popup-content">


        <?php $form = ActiveForm::begin(['class'=> 'sign-in']);?>

        <?php echo $form->field($model,'first_name')->textInput(['id'=>'ash','autofocus'=>true,])->label('Անուն'); ?>

        <?php echo $form->field($model,'last_name')->textInput(['autofocus'=>true])->label('Ազգանուն'); ?>

        <?php echo $form->field($model,'email')->textInput(['type'=>'email','id' => 'email','autofocus'=>true])->label('Էլ-հասցե'); ?>

        <?php echo  $form->field($model,'phone')->textInput(['autofocus'=>true])->label('Հեռախոսահամար'); ?>

        <?php echo  $form->field($model,'password')->textInput(['type' => 'password' ,'autofocus'=>true])->label('Գաղտնաբառ'); ?>
        <?php echo $form->field($model,'role')->textInput(['type' => 'hidden','class'=>'role' ,'autofocus'=>true,])->label('');?>

        <button type="submit" class="btn btn-primary btn-lg">Գրանցվել</button>
        <?php  $form = ActiveForm::end();?>


        <!--<form action="#" class="sign-in">
            <label for="email">Email:</label>
            <input type="text" id="email">
            <label for="password">Password:</label>
            <input type="password" id="password">
            <p class="check-mark">
                <input type="checkbox" id="remember-me">
                <label for="remember-me">Remember me</label>
            </p>
            <input type="submit" id="submit" value="Submit">
        </form>-->
        <!--<form action="#" class="register">
            <label for="email-register">Email:</label>
            <input type="text" id="email-register">
            <label for="password-register">Password:</label>
            <input type="password" id="password-register">
            <label for="password-confirmation">Confirm Password:</label>
            <input type="password-confirmation" id="password-confirmation">
            <p class="check-mark">
                <input type="checkbox" id="accept-terms">
                <label for="accept-terms">I agree to the <a href="#">Terms</a></label>
            </p>
            <input type="submit" id="submit" value="Create Account">
        </form>-->
    </div><!--.popup-content-->
</div><!--.main-popup-->

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>