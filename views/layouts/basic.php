<?php

use app\models\Courses;
use app\models\CourseStudent;
use yii\helpers\Html;
use app\assets\AppAsset;
use yii\bootstrap\Modal;
use yii\widgets\ActiveForm;
use app\models\Signup;
use app\models\User;
use app\controllers\SiteController;
use yii\helpers\Url;
use app\models\Users;

AppAsset::register($this);



$model = new Signup();

if(isset($_POST['Signup'])){
    $model->attributes = Yii::$app->request->post('Signup');
}

if($model->validate() && $model -> signup()){
    return $this->goHome();
}

$model1 = new Courses();
$query1 = Courses::find();
$courses = $query1->select('*')->all();
$courses_lenght = count($courses);

$user_model = User::find()->select('*')->limit(4);
$users = $user_model->all();

$user_model1 = User::find()->select('*');
$users1 = $user_model1->all();

$query2 = User::find()->select('*')->where(['role'=> 0]);
$student = $query2->all();
$student_count = count($student);

$query3 = User::find()->select('*')->where(['role'=> 1]);
$teachers = $query3->all();
$teachers_count = count($teachers);
?>

<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="keywords" content="">
	<!-- CSS -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="web/images/ico/logo.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body id="home" class="homepage">
<?php $this->beginBody() ?>

    <header id="header">
        <nav id="main-menu" class="navbar navbar-default navbar-fixed-top" role="banner">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.html"><img src="<?php echo Yii::$app->request->baseUrl.'/images/' . 'logo.png'?>" alt="logo"></a>
                </div>
				
                <div class="collapse navbar-collapse navbar-right">
                    <ul class="nav navbar-nav">
                        <li class="scroll active"><a href="#home">Գլխավոր</a></li>
                        <li class="scroll"><a href="#features">Առաջարկում ենք</a></li>
                        <li class="scroll"><a href="#services">Դասընթացներ</a></li>
                        <li class="scroll"><a href="#about">Մեր մասին</a></li>
                        <li class="scroll"><a href="#meet-team">Օգտատերեր</a></li>
                        <li class="scroll"><a href="#get-in-touch">Կապ</a></li>
                        <?php if (!Yii::$app->user->isGuest) : ?>
                            <li class="scroll"><a href="<?= Url::to(['site/logout'])?>" data-method="post">Ելք</a></li>
                        <?php else : ?>
                            <li class="scroll"><a href="<?php echo Yii::$app->request->baseUrl.'/site/login' ?>">Մուտք</a></li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div><!--/.container-->
        </nav><!--/nav-->
    </header><!--/header-->
    <section id="main-slider">
        <div class="owl-carousel">
            <div class="item" style="background-image: url(/web/images/slider/bg1.jpg);">
                <div class="slider-inner">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="carousel-content">
                                    <h2>Եթե ունես սովորելու <span>մեծ ցանկություն,</span>  ապա հայտնվել ես ճիշտ տեղում</h2>
                                    <button class="btn btn-primary btn-lg" id="role-stu"  data-toggle="modal" data-target="#myModal">Գրանցվել որպես ուսանող</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/.item-->
             <div class="item" style="background-image: url(/web/images/slider/bg2.jpg);">
                <div class="slider-inner">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="carousel-content">
                                    <h2>Եթե գիտակ ես և ունես <span>մեծ ցանկություն,</span> սովորեցնելու, կարող ես կիսվել քո գիտելիքներով</h2>
                                    <button class="btn btn-primary btn-lg" id="role-tech">Գրանցվել որպես դասավանդող</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--/.item-->
        </div><!--/.owl-carousel-->
    </section><!--/#main-slider-->

    <section id="cta" class="wow fadeIn">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2>Իմաստունը նա չԷ, ով շատ գիտի, այլ նա՝ ում գիտելիքներն օգտակար են:</h2>
                    <p style="text-align: right">Էսքիլես</p>
                </div>
            </div>
        </div>
    </section><!--/#cta-->

    <section id="features">
        <div class="container">
            <div class="section-header">
                <h2 class="section-title text-center wow fadeInDown">Առաջարկում ենք</h2>
                <p class="text-center wow fadeInDown">Մենք առաջարկում ենք հետևյալը՝</p>
            </div>
            <div class="row">
                <div class="col-sm-6 wow fadeInLeft">
                    <div class="fadein">
                        <img src="<?php echo Yii::$app->request->baseUrl.'/images/lern1.jpg' ?>">
                        <img src="<?php echo Yii::$app->request->baseUrl.'/images/lern2.jpg' ?>">
                        <img src="<?php echo Yii::$app->request->baseUrl.'/images/lern3.jpg' ?>">
                        <img src="<?php echo Yii::$app->request->baseUrl.'/images/lern4.jpg' ?>">
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="media service-box wow fadeInRight">
                        <div class="pull-left">
                            <i class="fa fa-graduation-cap"></i>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Սովորել</h4>
                        </div>
                    </div>

                    <div class="media service-box wow fadeInRight">
                        <div class="pull-left">
                            <i class="fa fa-briefcase"></i>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Կիսվել փորձով</h4>
                        </div>
                    </div>

                    <div class="media service-box wow fadeInRight">
                        <div class="pull-left">
                            <i class="fa fa-skype"></i>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Տեսազանգի հնարավորություն</h4>
                        </div>
                    </div>

                    <div class="media service-box wow fadeInRight">
                        <div class="pull-left">
                            <i class="fa fa-share-square"></i>
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">Նաև կարող եք կիսվել ձեր գաղափարներով և կարդալ նորություններ</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="services" >
        <div class="container">

            <div class="section-header">
                <h2 class="section-title text-center wow fadeInDown">Դասընթացներ</h2>
                <p class="text-center wow fadeInDown">Հետևյալ դասընթացները առաջարկվում են մեր կողմից <br> Կարող եք դասավանդել կամ սովորել։</p>
            </div>

            <div class="row">
                <div class="features">
                    <?php if($courses) : ?>
                        <?php for($i = 0; $i < $courses_lenght; $i++) : ?>
                        <div class="col-md-4 col-sm-6 wow fadeInUp" data-wow-duration="300ms" data-wow-delay="0ms">
                            <div class="courses_box btn-modal" data-val="popap-modal-contact-one" data-coursId="<?= $courses[$i]['id']?>">
                                <div class="courses_icon">
                                    <?= $courses[$i]['icon']?>
                                </div>
                                <div class="courses_info">
                                    <h5><?= $courses[$i]['name']?></h5>
                                    <p><?= $courses[$i]['description']?></p>
                                    <span>Տևողությունը՝</span> <?= $courses[$i]['duration'] .' ' . 'ամիս'?>
                                </div>
                            </div>
                        </div>
                        <?php endfor; ?>
                    <?php endif;?>


                </div>
            </div><!--/.row-->    
        </div><!--/.container-->
    </section><!--/#services-->
    <section id="about">
        <div class="container">

            <div class="section-header">
                <h2 class="section-title text-center wow fadeInDown">Մեր մասին</h2>
            </div>

            <div class="row">
                <div class="col-sm-6 wow fadeInLeft">
                    <h3 class="column-title">Ինչպես դառնալ ծրագրավորող՝ 5 խորհուրդ</h3>
                    <!-- 16:9 aspect ratio -->
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe  src="https://www.youtube.com/embed/jfxcJH_OWgE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>

                <div class="col-sm-6 wow fadeInRight">
                    <h3 class="column-title">Կոդավորիր ինձ հետ (C.W.M.)</h3>
                    <p>Այս կայքը նախատեսված է վեբ ծրագրավորման օնլայն դասընթացների համար։ Ներկայիս ժամանակաշրջանում համացանցը ողողված է ծրագրավորման և ինչու չէ նաև այլ ոլորտի ուսուցողական նյութերով`  դակումենտացիաներ, վիդեոդասեր և այլն։ Գաղտնիք չէ, որ ծրագրավորողների զգալի մասը դարձել են ծրագրավորող զբաղվելով ինքնակրթությամբ համացանցում։ Ես ինքս թեև մասնակցել եմ վեբ ծրագրավորման դասընթացների սակայն իմ գիտելիքների զգալի մասը ստացել եմ համացանցում՝ նյութեր կարդալով և վիդեոդասեր դիտելով։ Սակայն այդ ընթացքում հանդիպում ես դժվարությունների, կանգնում ես որոշ հարցերի առաջ և չես կրողանում կապվել տվյալ նյութի հեղինակին և տալ քեզ հետաքրքրող հարցերը։ Մեր կայքը ստեղծվել է հենց այդ նպատակով։ Այն հնարավորություն է տալիս գրանցվել որպես ուսանող կամ դասավանդող, ընտրել դասընթացներ և այն դասավանդողին՝ տեսնելով նրա էջը և կատարած աշխատանքները։ Մեր կայքը ապահովում է ուղիղ շփում դասավանդողի և ուսանողի միջև նաև տեսազանգի միջոցով։ Դասավանդողը կարող է ներկայացնել դասընթացի իր ծրագիրը, տալ առաջադրանքներ և պտասխանել ուսանողի հարցերին։ Այս համակարգը դարձնում է ուսուցումը ավելի արագ, կենտրոնացված, համակարգված և արդյունավետ՝ եթե քեզ դասավանդողը փորձ ունի և գիտի ճիշտ ուղություն ցույց տալ։ Մեկ մտքով ասած կայքը հնարավորություն է տալիս և՛ կիսվել փորձով, և՛ սովորել։</p>
                </div>
            </div>
        </div>
    </section><!--/#about-->

    <section id="work-process">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="section_title">
                        <h3>Այս պահին մեր կայքից օգտվում են</h3>
                    </div>
                </div>
            </div>

            <div class="row text-center">
                <div class="col-md-4 col-md-4 col-xs-6">
                    <div class="count_us wow fadeInUp" data-wow-duration="400ms" data-wow-delay="0ms">
                            <span><?=$student_count; ?></span>
                        <h3>Ուսանող</h3>
                    </div>
                </div>
                <div class="col-md-4 col-md-4 col-xs-6">
                    <div class="count_us wow fadeInUp" data-wow-duration="400ms" data-wow-delay="100ms">
                            <span><?=$teachers_count; ?></span>
                        <h3>Դասավանդող</h3>
                    </div>
                </div>
                <div class="col-md-4 col-md-4 col-xs-6">
                    <div class="count_us wow fadeInUp" data-wow-duration="400ms" data-wow-delay="200ms">
                            <span><?= count($users1); ?></span>
                        <h3>Ընդհանուր</h3>
                    </div>
                </div>
            </div>
        </div>
    </section><!--/#work-process-->

    <section id="meet-team">
        <div class="container">
            <div class="section-header">
                <h2 class="section-title text-center wow fadeInDown">Օգտատերեր</h2>
                <p class="text-center wow fadeInDown">Նրանք օգտվում են այս կայքից, հերթը քոնն է կարող ես գրացվել</p>
            </div>

            <div class="row">
                <?php if($users) : ?>
                <?php for($i = 0; $i < count($users); $i++) : ?>
                <div class="col-sm-6 col-md-3">
                    <div class="team-member wow fadeInUp" data-wow-duration="400ms" data-wow-delay="0ms">
                        <div class="team-img">
                            <?php if($users[$i]['prof_img'] ): ?>
                            <img class="img-responsive img-circle" src="<?php echo Yii::$app->request->baseUrl.'/uploads/'. $users[$i]['prof_img']?>" alt="">
                            <?php else: ?>
                                <img src="https://wowsciencecamp.org/wp-content/uploads/2018/07/dummy-user-img-1-400x400_x_acf_cropped.png" alt="">
                            <?php endif; ?>
                        </div>
                        <div class="team-info">
                            <h3><?=$users[$i]['first_name'] .' '.$users[$i]['last_name']  ?></h3>
                            <span><?php echo $users[$i]['role'] == '0' ?  'Ուսանող' : 'Դասավանդող';?></span>
                        </div>
                    </div>
                </div>
                    <?php endfor; ?>
                <?php endif; ?>

            </div>

            <div class="divider"></div>
        </div>
    </section><!--/#meet-team-->
    <section id="testimonial">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2">
                    <div id="carousel-testimonial" class="carousel slide text-center" data-ride="carousel">
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner" role="listbox">
                            <div class="item active">
                                <p><img class="img-circle img-thumbnail" src="<?php echo Yii::$app->request->baseUrl.'/images/testimonial/01.jpg'?>" alt=""></p>
                                <h4>Yetty L. Steven</h4>
                                <small>Nemo enim ipsam voluptatem quia voluptas sit</small>
                                <p>Aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est</p>
                            </div>
                            <div class="item">
                                <p><img class="img-circle img-thumbnail" src="<?php echo Yii::$app->request->baseUrl.'/images/testimonial/02.jpg'?>" alt=""></p>
                                <h4>Gerry Munstons</h4>
                                <small>Itaque earum rerum hic tenetur a sapiente delectus</small>
                                <p>Ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat. Sed ut perspiciatis unde omnis</p>
                            </div>
                        </div>
                        <!-- Controls -->
                        <div class="btns">
                            <a class="btn btn-primary btn-sm" href="#carousel-testimonial" role="button" data-slide="prev">
                                <span class="fa fa-angle-left" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span>
                            </a>
                            <a class="btn btn-primary btn-sm" href="#carousel-testimonial" role="button" data-slide="next">
                                <span class="fa fa-angle-right" aria-hidden="true"></span>
                                <span class="sr-only">Next</span>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!--/#testimonial-->
    <section id="contact">
        <div style="overflow:hidden;width: 1900px;position: relative;"><iframe width="1900" height="650" src="https://maps.google.com/maps?width=1900&amp;height=700&amp;hl=en&amp;q=%D5%80%D5%A1%D5%B5%D5%A1%D5%BD%D5%BF%D5%A1%D5%B6%2C%20%D4%B5%D6%80%D6%87%D5%A1%D5%B6+(C.W.M)&amp;ie=UTF8&amp;t=&amp;z=11&amp;iwloc=B&amp;output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe><div style="position: absolute;width: 80%;bottom: 10px;left: 0;right: 0;margin-left: auto;margin-right: auto;color: #000;text-align: center;"><small style="line-height: 1.8;font-size: 2px;background: #fff;">Powered by <a href="https://embedgooglemaps.com/fr/">https://embedgooglemaps.com/fr/</a> & <a href="https://iamsterdamcard.it">iamsterdamcard.it</a></small></div><style>#gmap_canvas img{max-width:none!important;background:none!important}</style></div><br />
        <div class="container-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4 col-sm-offset-8">
                        <div class="contact-form">
                            <h3>Կոնտակտային տվյալներ</h3>

                            <address>
                              <strong>Մեր հասցեն՝</strong><br>
                              Քաղաք Երևան<br>
                              Մոսկովյան 20/9<br>
                              <abbr title="Phone"></abbr> (010) 55-78-90
                            </address>

                            <form id="main-contact-form" name="contact-form" method="post" action="contact-us.send.php">
                                <div class="form-group">
                                    <input type="text" name="name" class="form-control" placeholder="Name" required>
                                </div>
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control" placeholder="Email" required>
                                </div>
                                <div class="form-group">
                                    <input type="text" name="subject" class="form-control" placeholder="Subject" required>
                                </div>
                                <div class="form-group">
                                    <textarea name="message" class="form-control" rows="8" placeholder="Message" required></textarea>
                                </div>
                                <button type="submit" class="btn btn-primary">Send Message</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!--/#bottom-->

    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    &copy; <?= date('Y') ?> C.W.M
                </div>
                <div class="col-sm-6">
                    <ul class="social-icons">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                        <li><a href="#"><i class="fa fa-flickr"></i></a></li>
                        <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer><!--/#footer-->

<div class="container">
</div>
<div class="overlay">
</div>
<div class="main-popup">
    <div class="popup-header">
        <div id="popup-close-button"><a href="#"></a></div>
        <ul>
            <li><a href="#" id="sign-in">Գրանցվել որպես ուսանող</a></li>
            <li><a href="#" id="register">Գրանցվել որպես Դասավանդող</a></li>
        </ul>
    </div><!--.popup-header-->
    <div class="popup-content">


        <?php $form = ActiveForm::begin(['class'=> 'sign-in']);?>

            <?php echo $form->field($model,'first_name')->textInput(['id'=>'ash',])->label('Անուն'); ?>

        <?php echo $form->field($model,'last_name')->textInput()->label('Ազգանուն'); ?>

        <?php echo $form->field($model,'email')->textInput(['type'=>'email','id' => 'email',])->label('Էլ-հասցե'); ?>

        <?php echo  $form->field($model,'phone')->textInput()->label('Հեռախոսահամար'); ?>

        <?php echo  $form->field($model,'password')->textInput(['type' => 'password'])->label('Գաղտնաբառ'); ?>
        <?php echo $form->field($model,'role')->textInput(['type' => 'hidden','class'=>'role' ])->label('');?>
            <div class="reg_button">
                <button type="submit" class="btn btn-primary btn-lg">Գրանցվել</button>
            </div>

        <?php  $form = ActiveForm::end();?>


        <!--<form action="#" class="sign-in">
            <label for="email">Email:</label>
            <input type="text" id="email">
            <label for="password">Password:</label>
            <input type="password" id="password">
            <p class="check-mark">
                <input type="checkbox" id="remember-me">
                <label for="remember-me">Remember me</label>
            </p>
            <input type="submit" id="submit" value="Submit">
        </form>-->
        <!--<form action="#" class="register">
            <label for="email-register">Email:</label>
            <input type="text" id="email-register">
            <label for="password-register">Password:</label>
            <input type="password" id="password-register">
            <label for="password-confirmation">Confirm Password:</label>
            <input type="password-confirmation" id="password-confirmation">
            <p class="check-mark">
                <input type="checkbox" id="accept-terms">
                <label for="accept-terms">I agree to the <a href="#">Terms</a></label>
            </p>
            <input type="submit" id="submit" value="Create Account">
        </form>-->
    </div><!--.popup-content-->
</div><!--.main-popup-->



    <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>