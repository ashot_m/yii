<?php

use app\models\UploadImage;
use yii\helpers\Html;
use app\assets\AppAsset;
use yii\bootstrap\Modal;
use yii\web\UploadedFile;
use yii\widgets\ActiveForm;
use app\models\Signup;
use app\models\User;
use app\controllers\SiteController;
use yii\db\ActiveRecord;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\filters\AjaxFilter;
use app\models\Courses;
use yii\web\JsExpression;
use app\models\Posts;
use app\models\CourseStudent;
use \app\models\CourseTeacher;
use app\models\Portfolio;
use app\models\Notification;


$user_id = Yii::$app->request->get('id');
$lodined_id = Yii::$app->user->id;


$CourseStudent = new CourseStudent();
$query3 = CourseStudent::find()->where(['student_id'=> $lodined_id]);
$res = $query3->all();
if(!$res){
    $flag = 0;
}else{
    $flag = 1;
}



$model = new UploadImage();
if(Yii::$app->request->isPost && Yii::$app->request->post('UploadImage')){
    $model->prof_img = UploadedFile::getInstance($model, 'prof_img');
    $model->upload();
}

$model1 = new Courses();
$query1 = Courses::find();
$courses = $query1->select('*')->all();
$courses_lenght = count($courses);

$user = new User();
$query = User::find();
$query->where(['id' => $user_id]);
$user_1 = $query->one();


$query_new = User::find();
$query_new->where(['id' => $lodined_id]);
$user_logined = $query->one();


$teachers = $query->where(['role' => '1'])->all();

AppAsset::register($this);
$post_model = new Posts();
if(Yii::$app->request->isPost){
    $post_model->cre();
}

$post_model1 = Posts::find();
$query2 = $post_model1->select('*');
$post = $query2->where(['c_user_id' => $user_id])->orderBy([ 'id' => SORT_DESC])->all();


$checked_courses = [];
for($q = 0; $q < count($res); $q++){
    $query5 = Courses::find()->select('*')->where(['id' => $res[$q]['course_id']]);
    $c = $query5->one();
    $checked_courses[] = $c;
}

$portfolio_model = new Portfolio();

$query9 = Portfolio::find()->select('*')->where(['teacher_id'=> $user_id]);
$portfolio_work = $query9->all();

$notifi_query = Notification::find()->select('*')->where(['to' => $lodined_id, 'seen_not_seen' => '0']);
$notification = $notifi_query->all();
$count_noti = count($notification);

$notifi_model = new Notification();
?>

<?php $this->beginPage(); ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <title><?= $user_1['first_name'] .' '. $user_1['last_name'] ?></title>
    <?php $this->head() ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?= Html::csrfMetaTags(); ?>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <!-- CSS -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="web/images/ico/logo.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->

<body id="home" class="homepage prof_home_page">
<?php $this->beginBody() ?>

<header id="header" class="prof_header">
    <nav id="main-menu" class="navbar navbar-default navbar-fixed-top" role="banner">
        <div class="container prof_cont">
            <div class="navbar-header prof-navbar-header">
                <a class="navbar-brand" href="<?php echo Yii::$app->request->baseUrl . '/site/profile?id='. $lodined_id ?>"><img src="<?php echo Yii::$app->request->baseUrl.'/images/' . 'logo.png'?>" alt="logo"></a>
            </div>
            <?php if($user_id == $lodined_id) : ?>
            <div class="header_right">
            <ul class="messige_ul">
                <li>
                    <a href="" class="messige_link">
                        <i class="fa fa-envelope"></i>
                    </a>
                </li>
                <li class="str_li">
                    <div class="notify" data-userid="<?php if($notification) echo $lodined_id; ?>">
                        <i class="fa fa-bell"></i>
                        <?php if($notification) : ?>
                        <span class="notifi_count"><?= $count_noti ?></span>
                        <?php endif; ?>
                    </div>
                    <ul class="notification_list">
                        <?php if($notification) : ?>
                        <?php for($l = 0; $l < $count_noti; $l++) : ?>
                            <?php if($notification[$l]['seen_not_seen'] == '0') : ?>
                        <li data-notid="<?= $notification[$l]['id'] ?>" class="noti_list">
                            <?php
                                $query10 = User::find()->select('first_name')->where(['id' => $notification[$l]['from']]);
                                $user_name = $query10->one();
                                $query11 = Courses::find()->select('name')->where(['id' => $notification[$l]['course_id']]);
                                $course_name = $query11->one();
                            ?>
                            <?php if($user_logined['role'] == '1') : ?>
                            <p class="notifi_text"><a href="<?php echo Yii::$app->request->baseUrl . '/site/profile?id='. $notification[$l]['from'] ?>"><?= $user_name->first_name ?></a> անունով ուսանողը ընտրել է ձեզ <strong><?= $course_name->name; ?></strong> դասընթացը դասավանդելու համար:</p>
                            <?php $form3 = ActiveForm::begin(['id' => 'notifi_form']) ?>
                                <?= $form3->field($notifi_model, 'notifi')->textarea(['placeholder'=>'Մերժելու դեպքում գրե՛ք պատճառը'])->label(''); ?>
                            <div class="notifi_buttons">
                                <button data-what="1" data-notifi="<?= $notification[$l]['id'] ?>" data-to1="<?= $notification[$l]['from'] ?>" data-cid1="<?=$notification[$l]['course_id']?>" class="notifi_button">Ընդունել</button>
                                <button data-what="0" data-notifi="<?= $notification[$l]['id'] ?>" data-to="<?= $notification[$l]['from'] ?>" data-cid="<?=$notification[$l]['course_id']?>" class="notifi_button">Չեղարկել</button>
                            </div>
                            <?php $form3 = ActiveForm::end() ?>
                            <?php else :?>
                                <?php if($notification[$l]['notifi'] !== '1') : ?>
                                <p class="notifi_text"><a href="<?php echo Yii::$app->request->baseUrl . '/site/profile?id='. $notification[$l]['from'] ?>"><?= $user_name->first_name ?></a> անունով դասախոսը մերժել է ձեզ <strong><?= $course_name->name; ?></strong> դասընթացը ձեզ դասավանդելու համար:</p>
                                <p>Պատճառը՝ <?= $notification[$l]['notifi'] ?></p>
                                <?php else :?>
                                    <p class="notifi_text"><a href="<?php echo Yii::$app->request->baseUrl . '/site/profile?id='. $notification[$l]['from'] ?>"><?= $user_name->first_name ?></a> անունով դասախոսը ընդունել է ձեր հրավերը <strong><?= $course_name->name; ?></strong> դասընթացը ձեզ դասավանդելու համար:</p>
                            <?php endif;?>
                            <?php endif;?>
                        </li>
                        <?php endif ?>
                        <?php endfor; ?>
                        <?php endif; ?>
                    </ul>
                </li>
            </ul>
            <ul class="prof_ul">
                <li class="pos">
                    <div class="prof_set">
                        <div class="prof_set_img">
                            <?php if($user_logined['prof_img'] ): ?>
                                <img src="/uploads/<?= $user_logined['prof_img'] ?>" alt="">
                            <?php else: ?>
                                <img src="https://wowsciencecamp.org/wp-content/uploads/2018/07/dummy-user-img-1-400x400_x_acf_cropped.png" alt="">
                            <?php endif; ?>
                        </div>

                        <div class="prof_name">
                            <p class="icon_name"><?= $user_logined['first_name'] .' '. $user_logined['last_name']?></p><i class="fa fa-sort-down"></i>
                        </div>
                    </div>
                    <ul class="prof_ul_none">
                        <li>
                            <a href="#">Կարգավորումներ</a>
                        </li>
                        <li>
                            <a href="<?= Url::to(['site/logout'])?>"> Դուրս գալ</a>
                        </li>
                    </ul>
                </li>
            </ul>
            <?php endif; ?>
            </div>
        </div><!--/.container-->
    </nav><!--/nav-->
</header><!--/header-->

<?php if($user_logined['role'] == '0') : ?>
<div class="profile_page">
    <div class="modal" id="myModal">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="my_img_up">
                    <!-- Modal Header -->
                    <div class="modal-header text-center">
                        <span>Թարմացնել անձնական էջի լուսանկարը</span>
                        <button type="button" class="modal_clos" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <?php $form = ActiveForm::begin() ?>
                        <?= $form->field($model, 'prof_img')->fileInput()->label('<i class="fa fa-camera" aria-hidden="true"></i>Ընտրել նկար') ?>
                        <button type="submit" class="update_img">Թարմացնել</button>
                        <?php ActiveForm::end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="profile_page_top">
            <div class="profile_page_cover">
                <img src="https://www.shutterstock.com/blog/wp-content/uploads/sites/5/2017/08/nature-design.jpg" alt="cover">
            </div>
            <div class="prof_img">
                <div class="profile_img_ramk">
                    <div class="profile_img_block">
                        <?php if($user_1['prof_img'] ): ?>
                            <img src="/uploads/<?= $user_1['prof_img'] ?>" alt="">
                        <?php else: ?>
                            <img src="https://wowsciencecamp.org/wp-content/uploads/2018/07/dummy-user-img-1-400x400_x_acf_cropped.png" alt="">
                        <?php endif; ?>
                        <button class="label_up" data-toggle="modal" data-target="#myModal"><i class="fa fa-camera"></i></button>
                    </div>
                </div>
                <div class="prof_name1">
                    <p>
                        <?= $user_1['first_name'] .' '. $user_1['last_name']?>
                    </p>
                    <?php if() ?>
                </div>
            </div>
        </div>
        <div class="prof_menu">
            <div class="prof_menu_content">
                <ul>
                    <li data-set="1" class="menu_li">Տեղեկություն</li>
                    <?php if($user_id == $lodined_id) : ?>
                    <li data-set="2" class="menu_li">Դասընթացներ</li>
                    <li data-set="3" class="menu_li">Իմ Դասընթացները</li>
                    <li data-set="4" class="menu_li">Դասախոսներ</li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>
    <section class="profile_content_section">
        <div class="container">
            <div class="default_page">
                <div class="default_page_content">
                    <?php if($user_id == $lodined_id) : ?>
                    <?php $form1 = ActiveForm::begin(['id' => 'no']) ?>
                        <?= $form1->field($post_model, 'post_desc')->textarea(['placeholder'=> 'Մտքիդ ի՞նչ կա․․․'])->label(''); ?>
                    <button type="submit">Կիսվել</button>
                    <?php $form1 = ActiveForm::end() ?>
                    <?php endif; ?>
                    <div class="posts_blog">
                        <?php if($post) : ?>
                            <?php for($k = 0; $k < count($post); $k++) : ?>
                            <div class="post_box">
                                <span class="post_text">
                                    <?= $post[$k]['post_desc'] ?>
                                </span>
                                <p><?= $post[$k]['created_at']?></p>
                            </div>
                            <?php endfor; ?>
                        <?php endif; ?>
                    </div>
                </div>
                <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2F%D5%8F%D5%8F-%D5%B8%D5%AC%D5%B8%D6%80%D5%BF%D5%AB-%D5%B6%D5%B8%D6%80%D5%B8%D6%82%D5%A9%D5%B5%D5%B8%D6%82%D5%B6%D5%B6%D5%A5%D6%80-1518089294994280%2F%3Fmodal%3Dadmin_todo_tour&tabs=timeline&width=400&height=1000&small_header=true&adapt_container_width=true&hide_cover=true&show_facepile=false&appId=616576692137092" width="400" height="1000" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
            </div>
            <div class="prof_info" data-set="1">
                <div class="block_title">
                    <h3></h3>
                    <?php if($user_id == $lodined_id) : ?>
                    <div class="edit_button">
                        <button data-toggle="modal" data-target="#myModal1"><i class="fa fa-edit"></i><span>Խնբագրել</span></button>
                    </div>
                    <?php endif; ?>
                </div>
                <div class="user_information">
                  <h6>Անձնական Տվյալներ`</h6>
                    <ul>
                        <li><p>Անուն ազգանուն</p> <span class="us_name"><?= $user_1['first_name'] .' '. $user_1['last_name']?></span></li>
                        <li><p>Բջջային հեռ․</p> <span class="phone"><?= $user_1['phone']?></span></li>
                        <li><p>Էլ-հասցե․</p> <span class="email"><?= $user_1['email']?></span></li>
                        <li><p>Պրոֆիլի տեսակ</p> <span class="role"><?php echo $user_1['role'] == '0' ?  'Ուսանող' : 'Դասավանդող';?></li>
                        <li><p>Նույանականացման համար</p> <span class="id"><?= $user_1['id'];?></span></li>
                    </ul>
                    <h6>Իմ մասին</h6>
                    <p class="about_me"><?= $user_1['about_me']?></p>
                </div>
            </div>
            <div class="prof_info" data-set="2">
                <div class="block_title">
                    <h3>

                    </h3>
                </div>
                <div class="courses_blog">
                    <div class="courses_blog_inner">
                        <?php if($courses) : ?>
                        <?php for($i = 0; $i < $courses_lenght; $i++) : ?>
                        <div class="courses_box btn-modal
                        <?php
                        $query4 = CourseStudent::find()->select('*')->where(['student_id'=> $lodined_id, 'course_id' => $courses[$i]['id']]);
                        $course_student = $query4->all();
                        if($course_student) echo 'checked_course';
                        ?>" data-val="popap-modal-contact-one" data-coursId="<?= $courses[$i]['id']?>">
                            <div class="courses_icon">
                                <?= $courses[$i]['icon']?>
                            </div>
                            <div class="courses_info">
                                <h5><?= $courses[$i]['name']?></h5>
                                <p><?= $courses[$i]['description']?></p>
                                <span>Տևողությունը՝</span> <?= $courses[$i]['duration'] .' ' . 'ամիս'?>
                            </div>
                        </div>
                         <?php endfor; ?>
                        <?php endif;?>
                    </div>
                </div>
            </div>
            <div class="prof_info" data-set="3">
                <div class="block_title">
                    <h3>

                    </h3>
                </div>
                <div class="my_courses_blog">
                    <div class="my_courses_inner">
                        <?php if($flag == 0) :?>
                            <div class="sms">
                                <p>Դուք դեռ չեք ընտրել որևէ դասընթաց</p>
                            </div>
                        <?php else :?>
                            <?php if($checked_courses) : ?>
                                <?php for($c = 0; $c < count($checked_courses); $c++) : ?>
                                    <div class="courses_box"  data-coursId="<?= $checked_courses[$c]['id']?>">
                                        <div class="courses_icon">
                                            <?= $checked_courses[$c]['icon']?>
                                        </div>
                                        <div class="courses_info">
                                            <h5><?= $checked_courses[$c]['name']?></h5>
                                            <span>Տևողությունը՝</span> <?= $checked_courses[$c]['duration'] .' ' . 'ամիս'?>
                                            <div class="course_maneger">
                                                <?php
                                                    $arr = [];
                                                    $x = [];
                                                        $query8 = \app\models\TeacherStudent::find()->select('*')->where(['student_id' => $lodined_id]);
                                                        $a = $query8->all();
                                                        for($k = 0; $k < count($a); $k++){
                                                            $query6 = CourseTeacher::find()->select('teacher_id')->where(['teacher_id'=> $a[$k]['teacher_id'], ]);
                                                            $checked_teacher_id = $query6->one();
                                                            $x[] = $checked_teacher_id;
                                                        }

                                                        for($f = 0; $f < count($x); $f++){
                                                            $query7 = User::find()->select(['first_name', 'last_name', 'id'])->where(['id'=> $x[$f]->teacher_id]);
                                                            $checked_teacher = $query7->one();
                                                            $arr[] = $checked_teacher;
                                                        }
                                                ?>
                                                <span>Դասավանդող՝ </span><a href="<?php echo Yii::$app->request->baseUrl . '/site/profile?id='. $arr[$c]->id ?>"><?=  $arr[$c]->first_name.' '.$arr[$c]->last_name; ?></a>
                                            </div>
                                         </div>
                                    </div>
                                <?php endfor; ?>

                            <?php endif;?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="prof_info" data-set="4">
                <div class="block_title">
                    <h3>

                    </h3>
                </div>
                <div class="teacher_blog_content">
                    <?php for($j = 0; $j < count($teachers); $j++) : ?>
                        <div class="teacher_box">
                            <div class="teacher_img">
                                <?php if($teachers[$j]['prof_img'] ): ?>
                                    <img src="/uploads/<?= $teachers[$j]['prof_img'] ?>" alt="">
                                <?php else: ?>
                                    <img src="http://municoncepcion.gob.pe/wp-content/uploads/2014/08/avatar-mini.png" alt="">
                                <?php endif; ?>
                            </div>
                            <div class="teacher_info">
                                <p><a href="<?php echo Yii::$app->request->baseUrl . '/site/profile?id='. $teachers[$j]['id'] ?>"><?= $teachers[$j]['first_name'].' ' . $teachers[$j]['last_name'] ?></a></p>
                                <span>Դասախոս</span>
                                <p class="profession">
                                </p>
                            </div>
                        </div>
                    <?php endfor; ?>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="modal" id="myModal1">
    <div class="modal-dialog">
        <div class="modal-content">

            <!-- Modal Header -->
            <div class="modal-header text-center">
                <span>Խմբագրել անձնական տվյալները</span>
                <button type="button" class="modal_clos mod1" data-dismiss="modal">&times;</button>
            </div>

            <!-- Modal body -->
            <div class="modal-body">
                <?php $form = ActiveForm::begin(['id'=> 'edit_form'])?>
                <?= $form->field($user, 'first_name')->textInput(['value' => $user_1['first_name']])->label('Անուն')?>
                <?= $form->field($user, 'last_name')->textInput(['value' => $user_1['last_name']])->label('Ազգանուն')?>
                <?= $form->field($user, 'email')->textInput(['value' => $user_1['email']])->label('Էլ-հասցե')?>
                <?= $form->field($user, 'phone')->textInput(['value' => $user_1['phone']])->label('Բջջային')?>
                <?= $form->field($user, 'about_me')->textarea(['value' => $user_1['about_me']])->label('Իմ մասին')?>
                <div class="save_edit_button">
                    <button  class="save_button">Պահպանել</button>
                </div>
                <?php $form = ActiveForm::end();?>
            </div>
        </div>
    </div>
</div>

<!--============popap-modal.contact============-->


<div id="popap-modal-contact-one" class="modal">
    <div class="modal-content">
        <div class="popap-modal-contact-info">
            <div class="close">
            </div>
            <div class="popap-modal-contact-min d_flex j_content_center a_items_center">
                <div class="popap-modal-contact-min-block d_flex j_content_between a_items_center">
                    <div class="modal-contact-nothing-found">
                        <div class="modal-contact-nothing-found-text">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--============popap-modal.contact============-->



<?php else : ?>





    <div class="profile_page">
        <div class="modal" id="myModal">
            <div class="modal-dialog">
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header text-center">
                        <span>Թարմացնել անձնական էջի լուսանկարը</span>
                        <button type="button" class="modal_clos" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <?php $form = ActiveForm::begin() ?>
                        <?= $form->field($model, 'prof_img')->fileInput() ?>
                        <button type="submit">Загрузить</button>
                        <?php ActiveForm::end() ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="profile_page_top">
                <div class="profile_page_cover">
                    <img src="https://www.shutterstock.com/blog/wp-content/uploads/sites/5/2017/08/nature-design.jpg" alt="cover">
                </div>
                <div class="prof_img">
                    <div class="profile_img_ramk">
                        <div class="profile_img_block">
                            <?php if($user_1['prof_img'] ): ?>
                                <img src="/uploads/<?= $user_1['prof_img'] ?>" alt="">
                            <?php else: ?>
                                <img src="https://wowsciencecamp.org/wp-content/uploads/2018/07/dummy-user-img-1-400x400_x_acf_cropped.png" alt="">
                            <?php endif; ?>
                            <button class="label_up" data-toggle="modal" data-target="#myModal"><i class="fa fa-camera"></i></button>
                        </div>
                    </div>
                    <div class="prof_name1">
                        <p>
                            <?= $user_1['first_name'] .' '. $user_1['last_name']?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="prof_menu">
                <div class="prof_menu_content">
                    <ul>
                        <li data-set="1" class="menu_li">Տեղեկություն</li>
                        <li data-set="4" class="menu_li">Պորտֆոլիո</li>
                        <?php if($user_id == $lodined_id) : ?>
                            <li data-set="2" class="menu_li">Դասընթացներ</li>
                            <li data-set="3" class="menu_li">Իմ Դասընթացները</li>
                        <?php endif; ?>
                    </ul>
                </div>
            </div>
        </div>
        <section class="profile_content_section">
            <div class="container">
                <div class="default_page">
                    <div class="default_page_content">
                        <?php if($user_id == $lodined_id) : ?>
                            <?php $form1 = ActiveForm::begin(['id' => 'no']) ?>
                            <?= $form1->field($post_model, 'post_desc')->textarea(['placeholder'=> 'Մտքիդ ի՞նչ կա․․․'])->label(''); ?>
                            <button type="submit">Կիսվել</button>
                            <?php $form1 = ActiveForm::end() ?>
                        <?php endif; ?>
                        <div class="posts_blog">
                            <?php if($post) : ?>
                                <?php for($k = 0; $k < count($post); $k++) : ?>
                                    <div class="post_box">
                                <span class="post_text">
                                    <?= $post[$k]['post_desc'] ?>
                                </span>
                                        <p><?= $post[$k]['created_at']?></p>
                                    </div>
                                <?php endfor; ?>
                            <?php endif; ?>
                        </div>
                    </div>
                    <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2F%D5%8F%D5%8F-%D5%B8%D5%AC%D5%B8%D6%80%D5%BF%D5%AB-%D5%B6%D5%B8%D6%80%D5%B8%D6%82%D5%A9%D5%B5%D5%B8%D6%82%D5%B6%D5%B6%D5%A5%D6%80-1518089294994280%2F%3Fmodal%3Dadmin_todo_tour&tabs=timeline&width=400&height=1000&small_header=true&adapt_container_width=true&hide_cover=true&show_facepile=false&appId=616576692137092" width="400" height="1000" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                </div>
                <div class="prof_info" data-set="1">
                    <div class="block_title">
                        <h3></h3>
                        <?php if($user_id == $lodined_id) : ?>
                            <div class="edit_button">
                                <button data-toggle="modal" data-target="#myModal1"><i class="fa fa-edit"></i><span>Խնբագրել</span></button>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="user_information">
                        <h6>Անձնական Տվյալներ`</h6>
                        <ul>
                            <li><p>Անուն ազգանուն</p> <span class="us_name"><?= $user_1['first_name'] .' '. $user_1['last_name']?></span></li>
                            <li><p>Բջջային հեռ․</p> <span class="phone"><?= $user_1['phone']?></span></li>
                            <li><p>Էլ-հասցե․</p> <span class="email"><?= $user_1['email']?></span></li>
                            <li><p>Պրոֆիլի տեսակ</p> <span class="role"><?php echo $user_1['role'] == '0' ?  'Ուսանող' : 'Դասավանդող';?></li>
                            <li><p>Նույանականացման համար</p> <span class="id"><?= $user_1['id'];?></span></li>
                        </ul>
                        <h6>Իմ մասին</h6>
                        <p class="about_me"><?= $user_1['about_me']?></p>
                    </div>
                </div>
                <div class="prof_info" data-set="2">
                    <div class="block_title">
                        <h3>

                        </h3>
                    </div>
                    <div class="courses_blog">
                        <p class="admin_sms">Ընտրեք այն դասընթացը, որը կարող եք դասավանդել որից հետո ձեր անունը կհայտնվի տվյալ դասընթացը դասավանդող ուսուցիչների ցուցակում, և ուսանողները կկարողանան ընտրել ձեզ։</p>
                        <p class="admin_sms_exam"><span>*</span> Ընտրելուց առաջ պետք է անցնեք թեստային քննություն:</p>
                        <div class="courses_blog_inner">
                            <?php if($courses) : ?>
                                <?php for($i = 0; $i < $courses_lenght; $i++) : ?>
                                    <div class="courses_box btn-modal
                        <?php
                                    $query4 = CourseTeacher::find()->select('*')->where(['teacher_id'=> $lodined_id, 'course_id' => $courses[$i]['id']]);
                                    $course_student = $query4->all();
                                    if($course_student) echo 'checked_course';
                                    ?>" data-val="popap-modal-contact-one" data-coursId="<?= $courses[$i]['id']?>">
                                        <div class="courses_icon">
                                            <?= $courses[$i]['icon']?>
                                        </div>
                                        <div class="courses_info">
                                            <h5><?= $courses[$i]['name']?></h5>
                                            <p><?= $courses[$i]['description']?></p>
                                            <span>Տևողությունը՝</span> <?= $courses[$i]['duration'] .' ' . 'ամիս'?>
                                        </div>
                                    </div>
                                <?php endfor; ?>
                            <?php endif;?>
                        </div>
                    </div>
                </div>
                <div class="prof_info" data-set="3">
                    <div class="block_title">
                        <h3>

                        </h3>
                    </div>
                    <div class="my_courses_blog">
                        <div class="my_courses_inner">
                            <?php if($flag == 0) :?>
                                <div class="sms">
                                    <p>Դուք դեռ չեք ընտրել որևէ դասընթաց</p>
                                </div>
                            <?php else :?>
                                <?php if($checked_courses) : ?>
                                    <?php for($c = 0; $c < count($checked_courses); $c++) : ?>
                                        <div class="courses_box"  data-coursId="<?= $checked_courses[$c]['id']?>">
                                            <div class="courses_icon">
                                                <?= $checked_courses[$c]['icon']?>
                                            </div>
                                            <div class="courses_info">
                                                <h5><?= $checked_courses[$c]['name']?></h5>
                                                <span>Տևողությունը՝</span> <?= $checked_courses[$c]['duration'] .' ' . 'ամիս'?>
                                                <div class="course_maneger">
                                                    <?php
                                                    $arr = [];
                                                    $x = [];
                                                    $query8 = \app\models\TeacherStudent::find()->select('*')->where(['student_id' => $lodined_id]);
                                                    $a = $query8->all();
                                                    for($k = 0; $k < count($a); $k++){
                                                        $query6 = CourseTeacher::find()->select('teacher_id')->where(['teacher_id'=> $a[$k]['teacher_id'], ]);
                                                        $checked_teacher_id = $query6->one();
                                                        $x[] = $checked_teacher_id;
                                                    }

                                                    for($f = 0; $f < count($x); $f++){
                                                        $query7 = User::find()->select(['first_name', 'last_name', 'id'])->where(['id'=> $x[$f]->teacher_id]);
                                                        $checked_teacher = $query7->one();
                                                        $arr[] = $checked_teacher;
                                                    }
                                                    ?>
                                                    <span>Դասավանդող՝ </span><a href="<?php echo Yii::$app->request->baseUrl . '/site/profile?id='. $arr[$c]->id ?>"><?=  $arr[$c]->first_name.' '.$arr[$c]->last_name; ?></a>
                                                </div>
                                            </div>
                                        </div>
                                    <?php endfor; ?>

                                <?php endif;?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
                <div class="prof_info" data-set="4">
                    <div class="block_title">
                        <h3>

                        </h3>
                    </div>
                    <div class="portfolio_blog">
                        <?php for($x=0; $x < count($portfolio_work); $x++) :?>
                        <div class="work_box">
                            <div class="work_info">
                                <h3><?=$portfolio_work[$x]['title']  ?></h3>
                                <i class="fa fa-align-right"></i> <span class="work_category"><?=$portfolio_work[$x]['category']  ?></span>
                                <p class="work_lunk">
                                    <i class="fa fa-link"></i> <a href="<?=$portfolio_work[$x]['link']  ?>"><?=$portfolio_work[$x]['link']  ?></a>
                                </p>
                                <div class="work_img">
                                    <img src="/uploads/portfolio/<?=$portfolio_work[$x]['portfolio_img']  ?>">
                                </div>
                                <p class="work_desc"><?=$portfolio_work[$x]['description']  ?></p>
                            </div>
                        </div>
                        <?php endfor; ?>
                        <?php if($lodined_id == $user_id) : ?>
                            <div class="portfolio_add" data-toggle="modal" data-target="#myModal2">
                                <i class="fa fa-plus-square"></i>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </section>
    </div>

    <div class="modal" id="myModal1">
        <div class="modal-dialog">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header text-center">
                    <span>Խմբագրել անձնական տվյալները</span>
                    <button type="button" class="modal_clos mod1" data-dismiss="modal">&times;</button>
                </div>
                <!-- Modal body -->
                <div class="modal-body">
                    <?php $form = ActiveForm::begin(['id'=> 'edit_form'])?>
                    <?= $form->field($user, 'first_name')->textInput(['value' => $user_1['first_name']])->label('Անուն')?>
                    <?= $form->field($user, 'last_name')->textInput(['value' => $user_1['last_name']])->label('Ազգանուն')?>
                    <?= $form->field($user, 'email')->textInput(['value' => $user_1['email']])->label('Էլ-հասցե')?>
                    <?= $form->field($user, 'phone')->textInput(['value' => $user_1['phone']])->label('Բջջային')?>
                    <?= $form->field($user, 'about_me')->textarea(['value' => $user_1['about_me']])->label('Իմ մասին')?>
                    <div class="save_edit_button">
                        <button  class="save_button">Պահպանել</button>
                    </div>
                    <?php $form = ActiveForm::end();?>
                </div>
            </div>
        </div>
    </div>



    <div class="modal" id="myModal2">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header text-center">
                    <span></span>
                    <button type="button" class="modal_clos mod1" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <div class="portfolio_form">
                        <?php $form2 = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'], 'id'=> 'add_portfolio']) ?>
                        <?= $form2->field($portfolio_model, 'title')->textInput()->label('Անվանում') ?>
                        <?= $form2->field($portfolio_model, 'category')->textInput()->label('Կատեգորիա') ?>
                        <?= $form2->field($portfolio_model, 'description')->textInput()->label('Նկարագրություն') ?>
                        <?= $form2->field($portfolio_model, 'link')->textInput()->label('Հղում') ?>
                        <?= $form2->field($portfolio_model, 'portfolio_img')->fileInput()->label('<i class="fa fa-camera"></i> Ավելացնել նկար') ?>
                        <div class="button_portfolio">
                            <button type="submit">Ավելացնել</button>
                        </div>
                        <?php $form2 = ActiveForm::end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!--============popap-modal.contact============-->


    <div id="popap-modal-contact-one" class="modal">
        <div class="modal-content">
            <div class="popap-modal-contact-info">
                <div class="close">
                </div>
                <div class="popap-modal-contact-min d_flex j_content_center a_items_center">
                    <div class="popap-modal-contact-min-block d_flex j_content_between a_items_center">
                        <div class="modal-contact-nothing-found">
                            <div class="modal-contact-nothing-found-text">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--============popap-modal.contact============-->







<?php endif; ?>

<?php
$js = <<<JS
$('#edit_form').on('beforeSubmit', function(){
    var data = $(this).serialize();
        $.ajax({
            url: '/web/site/edit',
            dataType: 'json',
            data: data,
            method: 'POST',
            success: function(res){
                console.log(res.first_name);
                $('.us_name').text(res.first_name + ' '+ res.last_name);
                $('.icon_name').text(res.first_name + ' '+ res.last_name);
                $('.phone').text(res.phone);
                $('.email').text(res.email);
                $('.about_me').text(res.about_me);
                $('.prof_name1 p').text(res.first_name + ' '+ res.last_name);
                $('#myModal1').css('display', 'none').attr('aria-hidden',"true").removeClass('in');
                $('#home').removeClass('modal-open').removeAttr('style')
                
            },
            error: function(){
                alert('Error!');
            }
        });
return false;
});
JS;

$this->registerJs($js);

$js1 = <<<JS
$('#no').on('beforeSubmit', function(){
    var data = $(this).serialize();
        $.ajax({
            url: '/web/site/add',
            dataType: 'json',
            data: data,
            timeout: 10000,
            method: 'POST',
            success: function(res){
                $('#no').after('<div class="post_box"><span>'+ res.post_desc +'</span><p>'+ res.post_date +'</p></div>');
                $('#posts-post_desc').val('');
            },
            error: function(){
                alert('Error!');
            }
        });
return false;
});
JS;


$this->registerJs($js1);

if($user_logined['role'] == '0') {


    $js2 = <<<JS
$('.courses_box').each(function() {
  if($(this).hasClass('checked_course'))
      {
        $(this).removeClass('btn-modal');
        $(this).removeAttr('data-val');
    }
})



$('.courses_box').on('click', function(){
    var id = $(this).data("coursid");
$.ajax({
    url: '/web/site/cours',
    dataType: 'json',
    data: {'course_id':id},
    method: 'POST',
    success: function(res){
    console.log(res.teachers);
    if(res.teachers === undefined){
        var html = '<span class="not_teacher">Դեռ ոչ մի դասախոս չի դասավանդում այս դասընթացը<span>';
        $('.modal-contact-nothing-found-text').html('<div class="course_blog"><div class="course_blog_img">'+ res.icon  +'</div><p class="course_name">'+ res.name  +'</p><p class="course_dur">Տևողությունը՝'+' '+  res.duration  +'ամիս</p><div class="course_title"><h3>Ինչ է՝ '+ res.name  +'-ը</h3></div><div class="desc_long"><p>'+ res.description_long +'</p></div><div class="teachers_list"><span></span>'+ html +'</div><div class="th"></div></div>')
    }else{
        var l = res.teachers.ids.length;
    var html = '<select name="teacher" id="check_teacher"><option></option>';
    for(var i = 0; i < l; i++ ){
        html = html + '<option value="'+ res.teachers.ids[i]+'">'+ res.teachers.names[i]+'</option>';
    }
    html += '</select>';
    $('.modal-contact-nothing-found-text').html('<div class="course_blog"><div class="course_blog_img">'+ res.icon  +'</div><p class="course_name">'+ res.name  +'</p><p class="course_dur">Տևողությունը՝'+' '+ res.duration  +'ամիս</p><div class="course_title"><h3>Ինչ է՝ '+ res.name  +'-ը</h3></div><div class="desc_long"><p>'+ res.description_long +'</p></div><div class="teachers_list"><label><span>Ընտրեք դասախոս </span>'+ html +'</label></div><div class="check_course_button_block"><button id="check_course_button" data-courseid="'+ res.id +'">Ընտրել</button></div><div class="th"></div></div>')
    }
    
    },
    error: function(){
    alert('Error!');
    }
    });
    });
JS;

    $this->registerJs($js2);

    $js3 = <<<JS
$(document).on('click','#check_course_button', function(){
    var cours_id = $(this).data('courseid');
    var teacher_id = $('#check_teacher').val();
    console.log(teacher_id);
    console.log(cours_id);
        $.ajax({
            url: '/web/site/check',
            dataType: 'json',
            data: {cours_id:cours_id, teacher_id:teacher_id},
            method: 'POST',
            success: function(res){
                if(res){
                    console.log(res);
                    $('.sms').remove();
                   $('.my_courses_inner').append('<div class="courses_box"  data-coursId=""><div class="courses_icon">'+ res.icon +'</div><div class="courses_info"><h5>'+ res.name +'</h5><span>Տևողությունը՝ </span>'+ res.duration +' <div class="course_maneger"><span>Դասավանդող՝ </span><a href="http://mysite.loc/web/site/profile?id='+ res.teacher_id +'">'+ res.teacher_name +'</a></div></div></div>'); 
                   $('#popap-modal-contact-one').css('display', 'none');
                $('.courses_box').each(function() {
                    if($(this).data('coursid') === res.course_id){
                        $(this).addClass('checked_course');
                        $(this).removeClass('btn-modal');
                        $(this).removeAttr('data-val');
                    }
                })
                }
            },
            error: function(){
                alert('Error!');
            }
        });
});
JS;


    $this->registerJs($js3);

}else{


    $js2 = <<<JS
$('.courses_box').each(function() {
  if($(this).hasClass('checked_course'))
      {
        $(this).removeClass('btn-modal');
        $(this).removeAttr('data-val');
    }
})



$('.courses_box').on('click', function(){
    var id = $(this).data("coursid");
    console.log(id);
$.ajax({
    url: '/web/site/test',
    dataType: 'json',
    data: {'course_id':id},
    method: 'POST',
    success: function(res){
        
        html = '<div class="test_blog">';
        
        for(var i = 0; i < res.questions.length; i++){
            html = html + '<div class="question_blog" data-id="'+ res.q_id[i] +'"><p class="question_text">'+ res.questions[i] +'</p>'
            for(var j = 0; j < res.answers[i].length; j++){
                html = html + '<div class="answers"><label><input class="checkbox_input" type="checkbox" value="'+ j +'">'+ res.answers[i][j] +'</label></div>'
            }
            html = html + '</div>';
        }
        
        html = html + '</div><button class="test_answer" data-cid="'+ id +'">Հաստատել</button>';
        
        $('.modal-contact-nothing-found-text').html(html);
    console.log(res.answers);
    },
    error: function(){
    alert('Error!');
    }
    });
    });
JS;
    $this->registerJs($js2);


    $js7 = <<<JS
$(document).on('click', '.test_answer', function(){
        var input = $('.checkbox_input');
        var arr = [];
        var id = $(this).data('cid');
        input.each(function() {
          if($(this).hasClass('a')){
            arr.push($(this).val());
        }
        });
        if(arr.length !== 10){
            $('.modal-contact-nothing-found-text').before('<p style="color:red" class="er_sms">Խնդրում ենք պատասխանել բոլոր հարցերին</p>');
            return false;
        }else{
            $('.er_sms').remove();
        }
        console.log(arr);
        $.ajax({
            url: '/web/site/verify',
            dataType: 'json',
            data: {arr:arr, course_id:id},
            method: 'POST',
            success: function(res){
                console.log(res.course_id);
                if(res.count === 10){
                    $('.modal-contact-nothing-found-text').html('<div class="ok verify"><i class="fa fa-check"></i>Շնորհավորում ենք դուք հաջողությամբ անցել եք թեստը</div>').addClass('dis');
                    $('.courses_box').each(function() {
                    if($(this).data('coursid') === parseInt(res.course_id) ){
                        $(this).addClass('checked_course');
                        $(this).removeClass('btn-modal');
                        $(this).removeAttr('data-val');
                    }
                })
                }else{
                    $('.modal-contact-nothing-found-text').html('<div class="ok "><i class="fa fa-times-circle"></i>Ցավոք դուք ունեք սխալ պատասխան(ներ), որի հետևանքով չեք կարող դասավանդել այս դասընթացը, վերանայեք ձեր գիտելիքները և կրկին փորձեք։</div>').addClass('dis');
                }
            },
            error: function(){
                alert('Error!');
            }
        });
return false;
});
JS;
    $this->registerJs($js7);



    $js5 = <<<JS
    
    $(document).on('click', '.checkbox_input', function() {
            var a = $(this);
            var arr = [];
            var id = $(this).parents('.question_blog').data('id');
            var val = $(this).val();
            arr = arr.push(val);
            if(!a.hasClass('a')){
                a.addClass('a');
            }else{
                a.removeClass('a'); 
            }
            
            console.log(arr);
    });


JS;
    $this->registerJs($js5);

    $js8 = <<<JS
$('.notifi_button').on('click', function(e){
    e.preventDefault();
    var noti_id = $(this).parents('.noti_list').data('notid');
    var this_nid = $(this).data('notifi');
    var list = $(this).parents('.noti_list');
    console.log(noti_id);
    
    if($(this).data('what') === 1){
        var sms1 = '1';
        var to1 = $(this).data('to1');
        var course_id1 = $(this).data('cid1');
        $.ajax({
            url: '/web/site/yes',
            dataType: 'json',
            data: {noti_id:noti_id, sms1:sms1, to1:to1, course_id1:course_id1},
            method: 'POST',
            success: function(res){
                list.remove();
            },
            error: function(){
                alert('Error!');
            }
        });
    }else{
        var sms = $(this).parents('.noti_list').find('.form-control').val();
        var to = $(this).data('to');
        var course_id = $(this).data('cid');
        console.log(sms);
        $.ajax({
            url: '/web/site/no',
            dataType: 'json',
            data: {noti_id:noti_id, sms:sms, to:to, course_id:course_id},
            method: 'POST',
            success: function(res){
                
            },
            error: function(){
                alert('Error!');
            }
        });
    }
        
return false;
});
JS;

    $this->registerJs($js8);




}



$js4 = <<<JS
$('#add_portfolio').on('beforeSubmit', function(e){
    e.preventDefault();
    console.log((new FormData(this)));
        $.ajax({
            url: '/web/site/add-portfolio',
            dataType: 'json',
            data: new FormData( this ),
            method: 'POST',
            processData: false,
            contentType: false,
            success: function(res){
                console.log(res);
                $('.portfolio_add').before('<div class="work_box"><div class="work_info"><h3>'+res.title+'</h3><i class="fa fa-align-right"></i><span class="work_category">'+res.category+'</span><p class="work_lunk"><i class="fa fa-link"></i><a href="'+res.link+'">'+res.link+'</a></p><div class="work_img"><img src="/uploads/portfolio/'+ res.portfolio_img +'"></div><p class="work_desc">'+res.desc +'</p></div></div>');
                $('#home').removeClass('modal-open').removeAttr('style');
                $('#myModal2').css('display', 'none').attr('aria-hidden', true).removeClass('in');
                $('.modal-backdrop').css({'height': '0', 'display' : 'none'}).removeClass('in').remove();
            },
            error: function(){
                alert('Error!');
            }
        });
return false;
});
JS;

$this->registerJs($js4);



?>




<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

