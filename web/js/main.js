jQuery(function($) {'use strict';

	// Navigation Scroll
	$(window).scroll(function(event) {
		Scroll();
	});

	$('.navbar-collapse ul li a').on('click', function() {  
		$('html, body').animate({scrollTop: $(this.hash).offset().top - 5}, 1000);
		return false;
	});

	// User define function
	function Scroll() {
		var contentTop      =   [];
		var contentBottom   =   [];
		var winTop      =   $(window).scrollTop();
		var rangeTop    =   200;
		var rangeBottom =   500;
		$('.navbar-collapse').find('.scroll a').each(function(){
			contentTop.push( $( $(this).attr('href') ).offset().top);
			contentBottom.push( $( $(this).attr('href') ).offset().top + $( $(this).attr('href') ).height() );
		})
		$.each( contentTop, function(i){
			if ( winTop > contentTop[i] - rangeTop ){
				$('.navbar-collapse li.scroll')
				.removeClass('active')
				.eq(i).addClass('active');			
			}
		})
	};

	$('#tohash').on('click', function(){
		$('html, body').animate({scrollTop: $(this.hash).offset().top - 5}, 1000);
		return false;
	});

	// accordian
	$('.accordion-toggle').on('click', function(){
		$(this).closest('.panel-group').children().each(function(){
		$(this).find('>.panel-heading').removeClass('active');
		 });

	 	$(this).closest('.panel-heading').toggleClass('active');
	});

	//Slider
	$(document).ready(function() {
		$('#HR').remove();
		var time = 7; // time in seconds

	 	var $progressBar,
	      $bar, 
	      $elem, 
	      isPause, 
	      tick,
	      percentTime;
	 
	    //Init the carousel
	    $("#main-slider").find('.owl-carousel').owlCarousel({
	      slideSpeed : 500,
	      paginationSpeed : 500,
	      singleItem : true,
	      navigation : true,
			navigationText: [
			"<i class='fa fa-angle-left'></i>",
			"<i class='fa fa-angle-right'></i>"
			],
	      afterInit : progressBar,
	      afterMove : moved,
	      startDragging : pauseOnDragging,
	      //autoHeight : true,
	      transitionStyle : "fadeUp"
			});
			
			
	 
	    //Init progressBar where elem is $("#owl-demo")
	    function progressBar(elem){
	      $elem = elem;
	      //build progress bar elements
	      buildProgressBar();
	      //start counting
	      start();
			}

			$(function(){
				$('.fadein img:gt(0)').hide();
				setInterval(function(){$('.fadein :first-child').fadeOut().next('img').fadeIn().end().appendTo('.fadein');}, 3000);
		});

	
	 
	    //create div#progressBar and div#bar then append to $(".owl-carousel")
	    function buildProgressBar(){
	      $progressBar = $("<div>",{
	        id:"progressBar"
	      });
	      $bar = $("<div>",{
	        id:"bar"
	      });
	      $progressBar.append($bar).appendTo($elem);
	    }
	 
	    function start() {
	      //reset timer
	      percentTime = 0;
	      isPause = false;
	      //run interval every 0.01 second
	      tick = setInterval(interval, 10);
	    };
	 
	    function interval() {
	      if(isPause === false){
	        percentTime += 1 / time;
	        $bar.css({
	           width: percentTime+"%"
	         });
	        //if percentTime is equal or greater than 100
	        if(percentTime >= 100){
	          //slide to next item 
	          $elem.trigger('owl.next')
	        }
	      }
	    }
	 
	    //pause while dragging 
	    function pauseOnDragging(){
	      isPause = true;
	    }
	 
	    //moved callback
	    function moved(){
	      //clear interval
	      clearTimeout(tick);
	      //start again
	      start();
	    }
	});

	//Initiat WOW JS
	new WOW().init();
	//smoothScroll
	smoothScroll.init();

	// portfolio filter
	$(window).load(function(){'use strict';
		var $portfolio_selectors = $('.portfolio-filter >li>a');
		var $portfolio = $('.portfolio-items');
		$portfolio.isotope({
			itemSelector : '.portfolio-item',
			layoutMode : 'fitRows'
		});
		
		$portfolio_selectors.on('click', function(){
			$portfolio_selectors.removeClass('active');
			$(this).addClass('active');
			var selector = $(this).attr('data-filter');
			$portfolio.isotope({ filter: selector });
			return false;
		});
	});

	$(document).ready(function() {
		//Animated Progress
		$('.progress-bar').bind('inview', function(event, visible, visiblePartX, visiblePartY) {
			if (visible) {
				$(this).css('width', $(this).data('width') + '%');
				$(this).unbind('inview');
			}
		});

		//Animated Number
		$.fn.animateNumbers = function(stop, commas, duration, ease) {
			return this.each(function() {
				var $this = $(this);
				var start = parseInt($this.text().replace(/,/g, ""));
				commas = (commas === undefined) ? true : commas;
				$({value: start}).animate({value: stop}, {
					duration: duration == undefined ? 1000 : duration,
					easing: ease == undefined ? "swing" : ease,
					step: function() {
						$this.text(Math.floor(this.value));
						if (commas) { $this.text($this.text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")); }
					},
					complete: function() {
						if (parseInt($this.text()) !== stop) {
							$this.text(stop);
							if (commas) { $this.text($this.text().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")); }
						}
					}
				});
			});
		};

		$('.animated-number').bind('inview', function(event, visible, visiblePartX, visiblePartY) {
			var $this = $(this);
			if (visible) {
				$this.animateNumbers($this.data('digit'), false, $this.data('duration')); 
				$this.unbind('inview');
			}
		});
	});

	// Contact form
	var form = $('#main-contact-form');
	form.submit(function(event){
		event.preventDefault();
		var form_status = $('<div class="form_status"></div>');
		$.ajax({
			url: $(this).attr('action'),
			beforeSend: function(){
				form.prepend( form_status.html('<p><i class="fa fa-spinner fa-spin"></i> Email is sending...</p>').fadeIn() );
			}
		}).done(function(data){
			form_status.html('<p class="text-success">Thank you for contact us.</p>').delay(3000).fadeOut();
		});
	});

	//Pretty Photo
	$("a[rel^='prettyPhoto']").prettyPhoto({
		social_tools: false
	});

	//Google Map
	var latitude = $('#google-map').data('latitude');
	var longitude = $('#google-map').data('longitude');
	function initialize_map() {
		var myLatlng = new google.maps.LatLng(latitude,longitude);
		var mapOptions = {
			zoom: 14,
			scrollwheel: false,
			center: myLatlng
		};
		var map = new google.maps.Map(document.getElementById('google-map'), mapOptions);
		var marker = new google.maps.Marker({
			position: myLatlng,
			map: map
		});
	}
	google.maps.event.addDomListener(window, 'load', initialize_map);

});

window.onload = function () {

	var error = Snap.select("#error" ),
		hole = Snap.select("#svg-hole" ),
		hand = Snap.select("#svg-hand" ),
		mask = Snap.select("#svg-mask" );


	function onSVGLoaded( ){

		function animOn(){
			hand.animate({
				transform: "t10,-300, r0"
			}, 4500, mina.easeinout, animOut);

			mask.animate({
				transform: "t-10,300, r0"
			}, 4500, mina.easeinout);
		}

		function animOut() {
			hand.animate({
				transform: "t-10,-305, r0"
			}, 4500, mina.easeinout, animOn);

			mask.animate({
				transform: "t10,305, r0"
			}, 4500, mina.easeinout);
		};

		function open() {
			clearTimeout(timer);
			hand.animate({
				transform: "t0,-300"
			}, 800, mina.backout, animOn);

			mask.animate({
				transform: "t0,300"
			}, 800, mina.backout);

		}
		timer = setTimeout(open, 1000);

		hand.attr({
			mask: mask
		});
	}

	onSVGLoaded();
};

/*Title: Cool Modal Popup Sign In/Out Form*/

$(function() {
	//defining all needed variables
	var $overlay = $('.overlay');
	var $mainPopUp = $('.main-popup')
	var $signIn = $('#sign-in');
	var $register = $('#register');
	var $formSignIn = $('form.sign-in');
	var $formRegister = $('form.register');

	var $firstChild = $('nav ul li:first-child');
	var $secondChild = $('nav ul li:nth-child(2)');
	var $thirdChild = $('nav ul li:nth-child(3)');

	//defining function to create underline initial state on document load
	function initialState() {
		$('.underline').css({
			"width": $firstChild.width(),
			"left": $firstChild.position().left,
			"top": $firstChild.position().top + $firstChild.outerHeight(true) + 'px'
		});
	}
	initialState(); //() used after calling function to call function immediately on doc load

	//defining function to change underline depending on which li is active
	function changeUnderline(el) {
		$('.underline').css({
			"width": el.width(),
			"left": el.position().left,
			"top": el.position().top + el.outerHeight(true) + 'px'
		});
	} //note: have not called the function...don't want it called immediately

	$firstChild.on('click', function(){
		var el = $firstChild;
		changeUnderline(el); //call the changeUnderline function with el as the perameter within the called function
		$secondChild.removeClass('active');
		$thirdChild.removeClass('active');
		$(this).addClass('active');
	});

	$secondChild.on('click', function(){
		var el = $secondChild;
		changeUnderline(el); //call the changeUnderline function with el as the perameter within the called function
		$firstChild.removeClass('active');
		$thirdChild.removeClass('active');
		$(this).addClass('active');
	});

	$thirdChild.on('click', function(){
		var el = $thirdChild;
		changeUnderline(el); //call the changeUnderline function with el as the perameter within the called function
		$firstChild.removeClass('active');
		$secondChild.removeClass('active');
		$(this).addClass('active');
	});


	$('button').on('click', function(){
		$overlay.addClass('visible');
		$mainPopUp.addClass('visible');
		$register.removeClass('active');
		$formRegister.removeClass('move-left');
		$formSignIn.removeClass('move-left');
	});
	$overlay.on('click', function(){
		$(this).removeClass('visible');
		$mainPopUp.removeClass('visible');
	});
	$('#popup-close-button a').on('click', function(e){
		e.preventDefault();
		$overlay.removeClass('visible');
		$mainPopUp.removeClass('visible');
	});

	$signIn.on('click', function(){
		$signIn.addClass('active');
		$register.removeClass('active');
		$formSignIn.removeClass('move-left');
		$formRegister.removeClass('move-left');
		$('#signup-role').val('0');
	});

	$register.on('click', function(){
		$signIn.removeClass('active');
		$register.addClass('active');
		$formSignIn.addClass('move-left');
		$formRegister.addClass('move-left');
		$('#signup-role').val('1');
	});

	$('input').on('submit', function(e){
		e.preventDefault(); //used to prevent submission of form...remove for real use
	});

	$('#role-stu').on('click', function () {
		$('#signup-role').val('0');
		$register.removeClass('active');
        $signIn.addClass('active');
	});

	$('#role-tech').on('click', function () {
		$('#signup-role').val('1');
		$signIn.removeClass('active');
        $register.addClass('active');
	})
});

$('.prof_ul').on('click', function () {
	$('.prof_ul_none').toggleClass('act');
});

$('.menu_li').on('click', function () {
	var block = $('.prof_info');
    var data = $(this).data('set');
    var title = $(this).text();
    block.removeClass('prof_cont_active');
    $('.default_page').css({'display' : 'none'});
	block.each(function () {
		if(data == $(this).data('set')){
			$(this).addClass('prof_cont_active');
			$(this).find('.block_title h3').text(title);
		}
	})

})

/*$('.save_button').on('click', function (event) {
	event.preventDefault();
	console.log('aaa');
	var data = $('#w1').serialize();
	$.ajax({
		url: '../../controllers/EditController',
		data: {data},
		type: 'POST',
		success: function () {
			console.log('yes');
		}
	})
})*/

$('.btn-modal').on('click', function () {
	var attr = $(this).attr('data-val');
	var modal = $('#' + attr);
	modal.removeClass('out')
	$('body').css({overflow: 'hidden '})
	modal.fadeIn()

})

$('.close').on('click', function () {
	var prt = $(this).parents('.modal');
	prt.addClass('out')

	setTimeout(function () {
		prt.fadeOut();
	}, 100)
	$('body').css({overflow: 'visible '})
})

$(window).on('click', function (event) {
	$('.modal').each(function () {
		var gtattr = $(this).attr('id');
		var new_mod = $('#' + gtattr);
		var md_cnt = $(new_mod).find('.modal-content')

		if (event.target === $(md_cnt)[0]) {
			setTimeout(function () {
				$(new_mod).addClass('out')
				$(new_mod).fadeOut()

			}, 100)
			$('body').css({overflow: 'visible '})
			$('.drp-seting').css({display: 'none'})
		}
		if (event.target === this) {
			setTimeout(function () {
				$(new_mod).addClass('out')
				$(new_mod).fadeOut()

			}, 100)
			$('body').css({overflow: 'visible '})
			$('.drp-seting').css({display: 'none'})

		}
	})
});

$('.personal-area-box-subscribe label p').click(function(){
	$('.personal-area-box-subscribe label p').removeClass("personal-area-box-active-text");
	$(this).addClass("personal-area-box-active-text");
});

$('.rating-min-hrefs-info ul li').click(function(){
	$('.rating-min-hrefs-info ul li').removeClass("rating-min-hrefs-info-active");
	$(this).addClass("rating-min-hrefs-info-active");
});

$(document ).on( 'change', '#check_teacher', function() {
	var id = $(this).val();
	var name = $("#check_teacher option:selected").text();
	console.log(name);
	if(id){
		$('.th').html('<div class="checked_teacher"><span>Դու կարող եք այցելել այս դասախոսի էջ՝ </span><a href="http://mysite.loc/web/site/profile?id= '+ id +'" target="_blank">'+ name +'</a></div>')
	}else{
		$('.th').empty();
	}

});

$('.notify').on('click', function () {
	$('.notification_list').toggleClass('show_noty')
})

$(document).ready(function () {
	$('#check_teacher').styler();

});

