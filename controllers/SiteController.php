<?php

namespace app\controllers;

use app\models\Answers;
use app\models\Courses;
use app\models\CourseStudent;
use app\models\Notification;
use app\models\Portfolio;
use app\models\PortfolioImage;
use app\models\Posts;
use app\models\TeacherStudent;
use app\models\Test;
use app\models\User;
use Yii;
use yii\base\Model;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\Signup;
use app\models\Login;
use app\models\UploadImage;
use yii\web\UploadedFile;
use app\models\CourseTeacher;



class SiteController extends Controller
{

    public $enableCsrfValidation = false;


    public function actionSay($message = 'Привет')
    {
        return $this->render('say', ['message' => $message]);
    }
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),

            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        $this->layout = 'error1';
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $model = new Signup();
        $this->layout = 'basic';
        //var_dump(Yii::$app->user->identity);die();
        return $this->render('index');

    }



    public function actionProfile()
    {
        $this->layout='basic-profile';
        return $this->render('profile');
    }

    public function actionChat()
    {
        $this->layout='basic-chat';
        return $this->render('chat');
    }



    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        /*if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);*/


        $this->layout = 'basic-login';
        $login_model = new Login();
        if(!Yii::$app->user->isGuest){
            return $this->goHome();
        }
        if(Yii::$app->request->post('Login')){
            $login_model->attributes = Yii::$app->request->post('Login');

            if($login_model->validate()){
                Yii::$app->user->login($login_model->getUser());
                $user_id = Yii::$app->user->id;
                return $this->redirect('profile'. '?id='. $user_id);
            }
        }


        return $this->render('login');
    }

    public function actionLogout()
    {
        if(!Yii::$app->user->isGuest){
            Yii::$app->user->logout();
            return $this->redirect(['login']);
        }
        return $this->render('logout');
    }






    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }


    public function actionUpload(){
        $model = new UploadImage();
        if(Yii::$app->request->isPost){
            $model->prof_img = UploadedFile::getInstance($model, 'prof_img');
            $model->upload();
        }
        return $this->render('upload', ['model' => $model]);
    }


    public function actionEdit(){
        $model_edit = new User();
        if($model_edit->load(Yii::$app->request->post())){
            $model_edit->edit();
            $arr = [
                'first_name' => $model_edit->first_name,
                'last_name' => $model_edit->last_name,
                'email' => $model_edit->email,
                'phone' => $model_edit->phone,
                'about_me' => $model_edit->about_me,
            ];
            print_r(json_encode($arr));die();
        }
    }

    public function actionAdd(){
        $model_add = new Posts();
        if($model_add->load(Yii::$app->request->post())){
            $model_add->cre();
            $date = date('Y-m-d H:i:s');
            $ar = [
                'post_desc'=> $model_add->post_desc,
                'post_date'=> $date
            ];
            print_r(json_encode($ar));die();
        }
    }

    public function actionCours()
    {
        $teacher_model = new User();
        $course_model = new Courses();
        if(Yii::$app->request->isAjax){
            $cours_id = Yii::$app->request->post('course_id');
            $res = $course_model->check($cours_id);
            $res1 = $teacher_model->checkTeacher($cours_id);
            if(!$res1){
                $array = [
                    'id' =>  $res->id,
                    'name' => $res->name,
                    'description' => $res->description,
                    'description_long' => $res->description_long,
                    'duration' => $res->duration,
                    'icon' => $res->icon,
                ];
                print_r(json_encode($array));die();
            }else {
                $array = [
                    'id' => $res->id,
                    'name' => $res->name,
                    'description' => $res->description,
                    'description_long' => $res->description_long,
                    'duration' => $res->duration,
                    'icon' => $res->icon,
                    'teachers' => $res1
                ];
                print_r(json_encode($array));die();
            }

        }


    }

    public function actionCheck()
    {
        $Ts_model = new TeacherStudent();
        $Cs_model = new CourseStudent();
        $Noty_model = new Notification();
        if(Yii::$app->request->isAjax){
            $cours_id = Yii::$app->request->post('cours_id');
            $teacher_id = Yii::$app->request->post('teacher_id');
            $Ts_model->TeacherStudent($teacher_id);
            $Cs_model->CourseStudent($cours_id);
            $Noty_model->addNoty($teacher_id, $cours_id);
            $query1 = User::find()->select('*')->where(['id'=> $teacher_id]);
            $teacher = $query1->one();
            $query = Courses::find()->select('*')->where(['id'=> $cours_id]);
            $res = $query->one();
            $arr = [
                'course_id' => $res->id,
                'name' => $res->name,
                'description' => $res->description,
                'duration' => $res->id,
                'icon' => $res->icon,
                'teacher_name' => $teacher->first_name .' '.$teacher->last_name,
                'teacher_id' => $teacher->id,
            ];
            print_r(json_encode($arr));die();
        }
    }

    public function actionAddPortfolio(){
        $model_addportfolio = new Portfolio();
        $up_model = new PortfolioImage();
        if($model_addportfolio->load(Yii::$app->request->post())){
            $up_model->portfolio_img = UploadedFile::getInstance($model_addportfolio, 'portfolio_img');
            $name = $up_model->upload();
            $model_addportfolio->add($name);
            $query = Portfolio::find()->select('id')->where(['title' => $model_addportfolio->title, 'portfolio_img'=> $name]);
            $work_id = $query->one();

            $ar = [
                'work_id'=> $work_id->id,
                'title'=> $model_addportfolio->title,
                'desc'=> $model_addportfolio->description,
                'category'=> $model_addportfolio->category,
                'link'=> $model_addportfolio->link,
                'portfolio_img' => $name,
            ];
            print_r(json_encode($ar));die();
        }
    }

    public function actionTest(){
        if(Yii::$app->request->isAjax){
            $course_id = Yii::$app->request->post('course_id');
            $query = Test::find()->select('*')->where(['course_id' => $course_id]);
            $tests = $query->all();
            $answers = [];
            $arr1 = [];
            $arr2 = [];
            $arr3 = [];
            $arr = [];

            $array = [[]];

            for($i = 0; $i < count($tests); $i++){
                $query1 = Answers::find()->select('*')->where(['question_id' => $tests[$i]->id])->all();
                $answers[] = $query1;
                $arr1[] = $tests[$i]->question;
                $arr3[] = $tests[$i]->id;
                for($j = 0; $j < count($answers[$i]); $j++){
                        $arr2[] = $answers[$i][$j]->answer;
                        $array[$i][$j] = htmlentities($answers[$i][$j]->answer);
                }
            }

            $arr = [
                'answers' => $array,
                'questions' => $arr1,
                'q_id' => $arr3
            ];

            print_r(json_encode($arr));die();
        }
    }

    public function actionVerify(){
        if(Yii::$app->request->post()){
            $arr = Yii::$app->request->post('arr');
            $id = Yii::$app->request->post('course_id');
            $query = Test::find()->select('*')->where(['course_id' => $id]);
            $tests = $query->all();
            $count = 0;
            for($i = 0; $i < count($arr); $i++){
                if($arr[$i] == $tests[$i]->right_answer){
                    $count += 1;
                }
            }

            if($count == 10){
                $query2 = new CourseTeacher();
                $user_id = Yii::$app->user->id;
                $query2->course_id = $id;
                $query2->teacher_id = $user_id;
                $query2->save();
                $arr = [
                    'count' => $count,
                    'course_id' => $id
                ];
                print_r(json_encode($arr));die();
            }else{
                print_r(json_encode(0));die();
            }

        }
    }

    public function actionYes(){
        $noti_model = new Notification();
        if(Yii::$app->request->isAjax){
            $sms = Yii::$app->request->post('sms1');
            $sms_to = Yii::$app->request->post('to1');
            $cours_id = Yii::$app->request->post('course_id1');
            $noti_id = Yii::$app->request->post('noti_id');
            $noti_model->sayYas($noti_id, $sms, $sms_to, $cours_id);
            print_r(json_encode('1'));die();
        }
    }

    public function actionNo(){
        $noti_model = new Notification();
        if(Yii::$app->request->isAjax){
            $noti_id = Yii::$app->request->post('noti_id');
            $sms = Yii::$app->request->post('sms');
            $sms_to = Yii::$app->request->post('to');
            $cours_id = Yii::$app->request->post('course_id');
            $noti_model->sayNo($noti_id, $sms, $sms_to, $cours_id);
            print_r(json_encode('1'));die();
        }
    }


}
